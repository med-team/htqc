#include "TestFramework.h"
#include "htio2/RefCounted.h"

using namespace htio2;

class TestType: public RefCounted
{
public:
    TestType(bool* flag): flag(flag) { *flag = true; }
    virtual ~TestType() { *flag = false; }
    bool* flag;
};

typedef SmartPtr<TestType> TestTypePtr;

void TestFramework::content()
{
    bool live_flag = false;
    TestType* obj = new TestType(&live_flag);

    IS(obj->get_ref_count(), size_t(0));
    IS(obj->get_ref_count(), obj->counter.get());
    OK(live_flag);

    obj->ref();
    IS(obj->get_ref_count(), size_t(1));

    {
        TestTypePtr p1(obj);
        IS(obj->get_ref_count(), size_t(2));
        TestTypePtr p2(p1);
        IS(obj->get_ref_count(), size_t(3));
        TestTypePtr p3(obj);
        IS(obj->get_ref_count(), size_t(4));
    }

    IS(obj->get_ref_count(), size_t(1));
    OK(live_flag);

    obj->unref();
    OK(!live_flag);
}
