#include "TestFramework.h"
#include "htio2/Kmer.h"

using namespace htio2;
using namespace std;

void TestFramework::content()
{
    for (char c = 'A'; c <= 'Z'; c++)
    {
        cout << "encode " << c << endl;
        IS(encode_aa(c), EncodedType(c-'A'));
    }

    for (EncodedType enc = 0; enc <= 25; enc++)
    {
        cout << "decode " << int(enc) << endl;
        IS(decode_aa(enc), 'A'+enc=='O' ? '*' : char('A'+enc));
    }

    {
        uint64_t manual_kmer =
                ('A'-'A') +
                ('F'-'A') * 26 +
                ('O'-'A') * 26 * 26 +
                ('C'-'A') * 26 * 26 * 26 +
                ('Z'-'A') * 26 * 26 * 26 * 26;
        IS(gen_kmer_aa<uint64_t>("AF*CZ"), manual_kmer);
    }

    {
        EncodedSeq enc;
        encode_aa("AF*KZ", enc);
        IS(enc.size(), size_t(5));
        IS(enc[0], EncodedType('A'-'A'));
        IS(enc[1], EncodedType('F'-'A'));
        IS(enc[2], EncodedType('O'-'A'));
        IS(enc[3], EncodedType('K'-'A'));
        IS(enc[4], EncodedType('Z'-'A'));

        vector<uint16_t> kmers;
        gen_kmer_aa(enc, kmers, 3);
        IS(kmers.size(), size_t(3));
        IS(kmers[0], uint16_t( ('A'-'A') + ('F'-'A')*26 + ('O'-'A')*26*26 ));
        IS(kmers[1], uint16_t( ('F'-'A') + ('O'-'A')*26 + ('K'-'A')*26*26 ));
        IS(kmers[2], uint16_t( ('O'-'A') + ('K'-'A')*26 + ('Z'-'A')*26*26 ));


        string dec;
        decode_aa(enc, dec);
        IS(dec, string("AF*KZ"));

        encode_aa("*", enc);
        IS(enc.size(), size_t(1));
        IS(enc[0], EncodedType('O'-'A'));

        decode_aa(enc, dec);
        IS(dec, string("*"));
    }
}
