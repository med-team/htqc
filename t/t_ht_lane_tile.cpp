#include "TestFramework.h"
#include "TestConfig.h"

#include "htio2/FastqIO.h"
#include "htio2/FastqSeq.h"

using namespace std;

#define BIN "../src/ht2-lane-tile"
#define INPUT TEST_SOURCE_DIR"/test_interleave.fastq.gz"

size_t count_seq(const string& file)
{
    size_t result = 0;
    htio2::FastqIO fh_accept(file, htio2::READ);
    htio2::FastqSeq seq;
    while (fh_accept.next_seq(seq))
        result++;
    return result;
}

void TestFramework::content()
{
    const char* cmd = BIN
            " -q"
            " -i " INPUT
            " -o lane_tile"
            " -u lane_tile_reject"
            " -T 1101";

    ok(!system(cmd), cmd);

    size_t n_accept = count_seq("lane_tile.fastq.gz");
    size_t n_reject = count_seq("lane_tile_reject.fastq.gz");

    IS(n_accept, 0);
    IS(n_reject, 200);
}
