#include "TestFramework.h"
#include "htio2/SeqIO.h"
#include "htio2/FastqSeq.h"
#include "htio2/PlainFileHandle.h"
#include "htio2/GzipFileHandle.h"

#include <iostream>

using namespace std;

TestFramework::TestFramework(): n_planned(0), n_got(0), n_fail(0)
{
}

TestFramework::TestFramework(size_t n_tests): n_planned(n_tests), n_got(0), n_fail(0)
{
    if (!n_tests)
    {
        cerr << "zero number of planned tests" << endl;
        exit(EXIT_FAILURE);
    }
}

TestFramework::~TestFramework()
{
}

bool TestFramework::run()
{
    content();
    if (n_planned && n_planned != n_got)
    {
        cerr << "planned " << n_planned << ", but only run " << n_got << endl;
        return false;
    }
    else
    {
        cout << n_got << " tests, " << n_fail << " failed" << endl;
        return (n_fail == 0);
    }
}

void TestFramework::ok(bool value, const char *desc)
{
    n_got++;
    if (value)
    {
        cout << "ok " << n_got << " - " << desc << endl;
    }
    else
    {
        cout << "NOT ok " << n_got << " - " << desc << endl;
        n_fail++;
    }
}

bool text_file_eq(const char* file1, const char* file2)
{
    string _file1_(file1);
    string _file2_(file2);
    bool success = true;

    htio2::FileHandle::Ptr fh1 = NULL;
    htio2::FileHandle::Ptr fh2 = NULL;

    if (_file1_.rfind(".gz") == _file1_.length()-3)
        fh1 = new htio2::GzipFileHandle(_file1_, htio2::READ);
    else
        fh1 = new htio2::PlainFileHandle(_file1_, htio2::READ);

    if (_file2_.rfind(".gz") == _file2_.length()-3)
        fh2 = new htio2::GzipFileHandle(_file2_, htio2::READ);
    else
        fh2 = new htio2::PlainFileHandle(_file2_, htio2::READ);

    string line1;
    string line2;

    while (1)
    {
        bool re1 = fh1->read_line(line1);
        bool re2 = fh2->read_line(line2);
        if (re1)
        {
            if (!re2)
            {
                success = false;
                break;
            }
        }
        else
        {
            if (re2)
            {
                success = false;
                break;
            }
            else
            {
                break;
            }
        }

        if (line1 != line2)
        {
            success = false;
            break;
        }
    }

    return success;
}

bool seq_file_id_eq(const char* file1, const char* file2)
{
    htio2::SeqIO::Ptr fh1(htio2::SeqIO::New(file1, htio2::READ));
    htio2::SeqIO::Ptr fh2(htio2::SeqIO::New(file2, htio2::READ));

    htio2::FastqSeq seq1, seq2;


    bool success = true;
    while (1)
    {
        bool re1 = fh1->next_seq(seq1);
        bool re2 = fh2->next_seq(seq2);

        if (re1)
        {
            if (!re2)
            {
                success = false;
                break;
            }
        }
        else
        {
            if (re2)
            {
                success = false;
                break;
            }
            else
            {
                break;
            }
        }

        if (seq1.id != seq2.id)
        {
            success = false;
            break;
        }
    }

    return success;
}

int main(int argc, char** argv)
{
    TestFramework t;
    bool t_re = t.run();
    exit(!t_re);
}
