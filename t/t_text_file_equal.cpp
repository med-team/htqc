#include "TestConfig.h"
#include "TestFramework.h"

using namespace std;

void TestFramework::content()
{
    const char* file1 = TEST_SOURCE_DIR"/test1.fastq.gz";
    const char* file2 = TEST_SOURCE_DIR"/test2.fastq";

    OK(text_file_eq(file1, file1));
    OK(text_file_eq(file2, file2));
    OK(!text_file_eq(file1, file2));
}
