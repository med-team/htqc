#include "TestFramework.h"
#include "htio2/SimpleSeq.h"

using namespace htio2;
using namespace std;

const char* SEQ = "TGGACGCAGCT";

void TestFramework::content()
{
    {
        string rev;
        revcom(SEQ, rev);
        IS(rev, string("AGCTGCGTCCA"));
    }

    SimpleSeq seq("seq1", "the quick brown fox jumps over a lazy dog", SEQ);
    IS(seq.id, string("seq1"));
    IS(seq.desc, string("the quick brown fox jumps over a lazy dog"));
    IS(seq.seq, string(SEQ));
    IS(seq.length(), size_t(11));

    {
        SimpleSeq rev;
        seq.revcom(rev);
        IS(rev.id, string("seq1"));
        IS(rev.desc, string("the quick brown fox jumps over a lazy dog"));
        IS(rev.seq, string("AGCTGCGTCCA"));
    }

    {
        string sub;
        seq.subseq(sub, 3);
        IS(sub, string("ACGCAGCT"));
        seq.subseq(sub, 3, 4);
        IS(sub, string("ACGC"));
    }

    {
        SimpleSeq sub;
        seq.subseq(sub, 3);
        IS(sub.id, string("seq1"));
        IS(sub.desc, string("the quick brown fox jumps over a lazy dog"));
        IS(sub.seq, string("ACGCAGCT"));

        seq.subseq(sub, 3, 4);
        IS(sub.seq, string("ACGC"));
    }
}
