//     HTQC - a high-throughput sequencing quality control toolkit
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef HTQC_FASTQSTAT_H
#define HTQC_FASTQSTAT_H


#include <htio2/HeaderUtil.h>
#include <htio2/FastqSeq.h>

#include "htqc/Common.h"
#include "htqc/BaseQualCounter.h"

namespace htqc
{

class FastqStat
{
public:
    FastqStat(bool mask, htio2::QualityEncode encode, htio2::HeaderFormat h_format, bool h_sra);

    ~FastqStat();

    void record_seq(htio2::FastqSeq& seq);
    FastqStat& operator+=(const FastqStat& other);

    void try_expand_store(size_t new_size);

public:
    htio2::HeaderParser header_parser;

    htio2::QualityEncode encode;
    int observed_max_len;
    int16_t qual_from;
    int16_t qual_to;
    bool qual_mask;

    std::vector<float> read_quals;
    std::vector<BaseQualCounter> base_qual_counter; // cycle => object
    std::vector<BaseCounter> base_compo_counter; // cycle => object
    BaseQualCounter read_qual_counter;
    std::vector<long> read_len_counter; // length => count
    std::map< int, std::map<int, TileQualCounter> > tile_read_qual_counter; // lane => tile => quality_range => count


} CACHE_LINE_ALIGNMENT;

}

#endif // HTQC_FASTQSTAT_H
