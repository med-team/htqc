#include "htio2/Version.h"
#include "htio2/config.h"

#include <sstream>

using namespace std;

namespace htio2
{

std::string get_version_string()
{
    ostringstream result;
    result << HTIO_VERSION_MAJOR << "." << HTIO_VERSION_MINOR << "." << HTIO_VERSION_PATCH;
    return result.str();
}

} // namespace htio2
