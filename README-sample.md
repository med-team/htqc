ht2-sample - randomly pick some reads
====================================

Introduction
------------

Sometimes you may want to randomly pick a fraction of sequences. ht2-sample
picks a given fraction of sequences randomly.

Note: the fraction is not exact. For example, if you have 1000 sequences and
specified to pick 1/10 of them, you will **NOT** get exactly 100 sequences.
This is a compromise to get better run-time performance.

Input and output
----------------

This program accepts both single-end and paired-end reads. When paired-end reads
is processed, the number of input files should be even, and the files of read 1
should be given on front, then follwing the files of read 2.

Usage
-----

Pick 1/100 of sequences, output will be written to sample.fastq:

  $ ht2-sample -S -i in.fastq -o sample -r 100

For paired-end, output will be written to sample_1.fastq and sample_2.fastq:

  $ ht2-sample -P -i in_1.fastq in_2.fastq -o sample -r 100
