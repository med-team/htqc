#include "TestFramework.h"
#include "TestConfig.h"
#include "htio2/SeqIO.h"
#include "htio2/FastqSeq.h"
#include <stdio.h>

using namespace std;
using namespace htio2;

void TestFramework::content()
{
    htio2::SeqIO* fh = SeqIO::New(TEST_SOURCE_DIR"/test1.fastq", htio2::READ);

    FastqSeq seq1;

    off_t off1 = fh->tell();
    IS(off1, 0);

    OK(fh->next_seq(seq1));
    IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
    IS(seq1.desc, "1:N:0:CTTGTA");
    IS(seq1.seq, "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
    IS(seq1.quality, "=?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA?ABCAC@9@AA8??8><ADB<>B8<B:C?>@>>34>:@CD");

    FastqSeq seq2;

    off_t off2 = fh->tell();
    IS(off2, 360);

    OK(fh->next_seq(seq2));
    IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:1197:2208");
    IS(seq2.desc, "1:N:0:TTGGTA");
    IS(seq2.seq, "AGCCTACGGGAGGCAGCAGTGGGGAATATTGCACAATGGAGGAAACTCTGATGCAGCGACGCCGCGTGAGTGAAGAAGTGGTTCGCTATGTAAAGCTCTATCAGCAGGGAAGATNNTGACGGTACCTGACTAAGAAGCTCCGGCTAACTA");
    IS(seq2.quality, "@@CFFDFFGHHHHJJHFHGHHGIIHIJJJJIHHJJJJGIGIJHIJGIICFC@ECEHAB@4=;@DBB8B@B:@:>C>>CCCD:?CDDDDBDEEEDCCDCDDCCCC@@ADD?BDBD##++8?@B9A9CDCCCCCDDCDCCD@C>BBDDCDDD");

    // absolute seek
    {
        FastqSeq seq;
        fh->seek(off1);
        OK(fh->next_seq(seq));
        IS(seq.id, seq1.id);
        IS(seq.desc, seq1.desc);
        IS(seq.seq, seq1.seq);
        IS(seq.quality, seq1.quality);
    }

    // relative seek
    {
        FastqSeq seq;
        fh->seek(off1);
        fh->seek(off2-off1, SEEK_CUR);
        OK(fh->next_seq(seq));
        IS(seq.id, seq2.id);
        IS(seq.desc, seq2.desc);
        IS(seq.seq, seq2.seq);
        IS(seq.quality, seq2.quality);
    }

    // end seek
    {
        FastqSeq seq;
        fh->seek(-360, SEEK_END);
        OK(fh->next_seq(seq));
        IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
        IS(seq.desc, "1:N:0:TTAGGC");
        IS(seq.seq, "ATCGCAAATCATGGTGGGGATGATACGGCGTTTCATTGCGGAGCAGGAAATTGTTATGTTCTTATCAGGAGATTATAATTGGCCTGACAAATGGATGTGATTTATTTATTGGGCTCAATTGGGCTAGGTTGGGGATCTTCATTTGTCCAC");
        IS(seq.quality, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
    }

    delete fh;
}

