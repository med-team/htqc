//     HTQC - a high-throughput sequencing quality control toolkit
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "htqc/Common.h"
#include "htio2/JUCE-3.0.8/JuceHeader.h"

using namespace std;
using namespace htio2;

namespace htqc
{
const int LOG_BLOCK = 50000;
const size_t NUM_GUESS = 500;

std::string bool_to_string(bool value)
{
    return value ? "T" : "F";
}

void check_output_overwrite(const std::vector<std::string>& files_in,
                            const std::string& file_out)
{
    for (size_t i = 0; i < files_in.size(); i++)
    {
        if (juce::File(files_in[i]) == juce::File(file_out))
        {
            cerr << "output file \"" << file_out << "\" will overwrite input file \"" << files_in[i] << "\"" << endl;
            exit(EXIT_FAILURE);
        }
    }
}

void separate_paired_files(const std::vector<std::string>& in, std::vector<std::string>& names_a, std::vector<std::string>& names_b)
{
    size_t size = in.size();

    if (size % 2)
    {
        cerr << "ERROR: odd number of files for paired-end mode: " << size << endl;
        for (int i = 0; i < size; i++)
        {
            cerr << "  " << in[i] << endl;
        }
        exit(EXIT_FAILURE);
    }

    int half = size / 2;

    for (int i = 0; i < half; i++)
        names_a.push_back(in[i]);

    for (int i = half; i < size; i++)
        names_b.push_back(in[i]);
}

void add_fastq_suffix_se(const std::string& prefix,
                         htio2::CompressFormat comp,
                         std::string& file)
{
    file = prefix + ".fastq";
    if (comp == htio2::COMPRESS_GZIP)
        file += ".gz";
}

void add_fastq_suffix_pe(const std::string& prefix,
                         htio2::CompressFormat comp,
                         std::string& file1,
                         std::string& file2)
{
    file1 = prefix + "_1.fastq";
    file2 = prefix + "_2.fastq";
    if (comp == htio2::COMPRESS_GZIP)
    {
        file1 += ".gz";
        file2 += ".gz";
    }
}

} // namespace htqc






