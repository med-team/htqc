#include "TestConfig.h"
#include "TestFramework.h"

void TestFramework::content()
{
    {
        const char* cmd = "../src/ht2-trim --quiet -i " TEST_SOURCE_DIR "/test1.fastq.gz -S head -o trim_head";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/trim_head.fastq.gz", "trim_head.fastq.gz"));
    }

    {
        const char* cmd = "../src/ht2-trim --quiet -i " TEST_SOURCE_DIR "/test1.fastq.gz -S tail -o trim_tail";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/trim_tail.fastq.gz", "trim_tail.fastq.gz"));
    }

    {
        const char* cmd = "../src/ht2-trim --quiet -i " TEST_SOURCE_DIR "/test1.fastq.gz -o trim_both";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/trim_both.fastq.gz", "trim_both.fastq.gz"));
    }
}

