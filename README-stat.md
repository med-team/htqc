ht2-stat - summarize sequencing reads quality
=============================================

Introduction
------------

`ht2-stat` produces sequence assessment in following aspects:

  - quality by cycle
  - quality by lane/tile
  - base composition by cycle
  - quality histogram
  - length histogram
  - quality QQ plot for paired-end reads

Input and output
----------------

The program accepts both single-end and paired-end reads. When paired-end reads
is processed, the number of input files should be even, and the files of read
1 should be given on front, then follwing the files of read 2.

Results are written to a directory in plain-text tab-delimited format. If input
is paired-end, the results for each end are placed in separated files.

Usage
-----

Output files are placed in "qual_stat" folder:

    $ ht2-stat -S -i in.fastq -o qual_stat

For paired-end reads:

    $ ht2-stat -P -i in_1.fastq in_2.fastq -o qual_stat
