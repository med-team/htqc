ht2-filter - filter reads by different criteria
===============================================

Introduction
------------

The program `ht2-filter` filters sequences according to the length of high
quality bases.

Input and output
----------------

Both single-end and paired-end reads are accepted. When paired-end reads is
processed, the number of input files should be even, and the files of read 1
should be given on front, then follwing the files of read 2.

When input reads are paired-end and only one end is accepted, it will be
written to a separate file (OutputPrefix_s.fastq).

Usage
-----

The accepted reads will be stored in file OUT_1.fastq and OUT_2.fastq, and the
one-end-accepted reads will be stored in file OUT_s.fastq.

    $ ht2-filter --paired-end -i IN_1.fastq IN_2.fastq -o OUT
