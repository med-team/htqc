#include "TestFramework.h"
#include "TestConfig.h"
#include "htio2/PlainFileHandle.h"
#include <stdio.h>


using namespace std;
using namespace htio2;

void TestFramework::content()
{
    PlainFileHandle fh(TEST_SOURCE_DIR"/test1.fastq", htio2::READ);
    string line1;
    string line2;
    string line3;
    string line4;

    OK(fh.read_line(line1));
    off_t off1 = fh.tell();
    OK(fh.read_line(line2));
    off_t off2 = fh.tell();
    OK(fh.read_line(line3));
    off_t off3 = fh.tell();
    OK(fh.read_line(line4));
    off_t off4 = fh.tell();

    IS(line1, "@HWI-ST1106:815:H154GADXX:1:1101:1244:2196 1:N:0:CTTGTA");
    IS(off1, 56);
    IS(line2, "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
    IS(off2, 207);
    IS(line3, "+");
    IS(off3, 209);
    IS(line4, "=?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA?ABCAC@9@AA8??8><ADB<>B8<B:C?>@>>34>:@CD");
    IS(off4, 360);
    IS(ftell(fh.handle), 361);

    fh.seek(off3, 0);
    string line4again;
    OK(fh.read_line(line4again));
    IS(line4again, line4);

    fh.seek(off1, 0);
    string line2again;
    OK(fh.read_line(line2again));
    IS(line2again, line2);

    fh.seek(off2, SEEK_SET);
    fh.seek(off3-off2, SEEK_CUR);
    IS(fh.tell(), off3);
    IS(ftell(fh.handle), off3+1);
    {
        string line;
        OK(fh.read_line(line));
        IS(line, line4);
    }

    fh.seek(-151, SEEK_END);
    IS(fh.tell(), 36000-151);
    {
        string line;
        OK(fh.read_line(line));
        IS(line, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
    }
}
