//     HTQC - a high-throughput sequencing quality control toolkit
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTQC_OPTIONS_H
#define	HTQC_OPTIONS_H

#include "htqc/Common.h"
#include "htio2/QualityUtil.h"
#include "htio2/HeaderUtil.h"
#include "htio2/OptionParser.h"


namespace htqc
{

#define OPTION_GROUP_IO "Input/Output Options"
#define OPTION_GROUP_MISC "Miscellaneous Options"
#define OPTION_GROUP_FORMAT "Sequence Format Options"

extern std::vector<std::string> OPT_files_in;
extern htio2::Option OPT_files_in_ENTRY;
void show_files_in();
void ensure_files_in();


#ifdef USE_prefix_out
extern std::string OPT_prefix_out;
extern htio2::Option OPT_prefix_out_ENTRY;
void show_prefix_out();
void ensure_prefix_out();
#endif

#ifdef USE_se_pe
extern bool OPT_se;
extern bool OPT_pe;
extern bool OPT_pe_itlv;
extern std::string _OPT_pe_strand;

extern htio2::Option OPT_se_ENTRY;
extern htio2::Option OPT_pe_ENTRY;
extern htio2::Option OPT_pe_itlv_ENTRY;
extern htio2::Option OPT_pe_strand_ENTRY;

htio2::Strand _cast_strand_chr(char value);
htio2::Strand OPT_pe_strand1();
htio2::Strand OPT_pe_strand2();


inline bool check_strand_chr(char value)
{
    switch (value)
    {
    case 'F':
    case 'f':
    case 'R':
    case 'r':
        return true;
    default:
        return false;
    }
}

void show_se_pe();
void ensure_se_pe();
#endif

#ifdef USE_encode
extern htio2::QualityEncode OPT_encode;
extern bool OPT_mask;
extern htio2::Option OPT_encode_ENTRY;
extern htio2::Option OPT_mask_ENTRY;
void show_encode();
void ensure_encode();
void show_mask();
#endif

#ifdef USE_header_format
extern htio2::HeaderFormat OPT_header_format;
extern bool OPT_header_sra;
extern htio2::Option OPT_header_format_ENTRY;
extern htio2::Option OPT_header_sra_ENTRY;
void show_header_format();
void ensure_header_format();
#endif

extern bool OPT_quiet;
extern htio2::Option OPT_quiet_ENTRY;

extern htio2::CompressFormat OPT_compress;
htio2::CompressFormat guess_compress_format_by_files_name(const std::vector<std::string>& files);

extern bool OPT_help;
extern htio2::Option OPT_help_ENTRY;

extern bool OPT_version;
extern htio2::Option OPT_version_ENTRY;

void tidy_prefix_out(std::string& prefix);

void show_version_and_exit();

void separate_paired_files(const std::vector<std::string>& in, std::vector<std::string>& names_a, std::vector<std::string>& names_b);

void remove_suffix(std::string& name, const std::string& suffix);

} // namespace htqc

#endif	/* HTQC_OPTIONS_H */

