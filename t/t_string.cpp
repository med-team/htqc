#include "TestFramework.h"
#include "htio2/StringUtil.h"

#include <vector>

using namespace std;

void TestFramework::content()
{
    {
        vector<string> result;
        htio2::split("aaa\tbbb\tccc", '\t', result);
        IS(result.size(), 3);
        IS(result[0], "aaa");
        IS(result[1], "bbb");
        IS(result[2], "ccc");
    }

    {
        vector<string> result;
        htio2::split("\taaa\tbbb\t", '\t', result);
        IS(result.size(), 4);
        IS(result[0], "");
        IS(result[1], "aaa");
        IS(result[2], "bbb");
        IS(result[3], "");
    }

    {
        vector<string> result;
        htio2::split("\t", '\t', result);
        IS(result.size(), 2);
        IS(result[0], "");
        IS(result[1], "");
    }

    {
        vector<string> result;
        htio2::split("aaa", '\t', result);
        IS(result.size(), 1);
        IS(result[0], "aaa");
    }

    {
        vector<string> result;
        htio2::split("", '\t', result);
        IS(result.size(), 0);
    }

    {
        vector<string> input;
        input.push_back("aaa");
        input.push_back("bbb");
        input.push_back("ccc");
        string result = htio2::join("abc", input.begin(), input.end());
        IS(result, "aaaabcbbbabcccc");
    }
}
