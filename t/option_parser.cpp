#include "htio2/OptionParser.h"

#include <vector>
#include <string>
#include <iostream>

using namespace std;
using namespace htio2;


int main(int argc, char** argv)
{
    bool opt1 = false;
    string opt2 = "opt2_default";
    vector<int> opt3;

    Option opt1_entry("foo", 'f', "help doc group 1",
                      &opt1, Option::FLAG_NONE,
                      "boolean option foo the quick brown fox jumps over a lazy dog, the quick brown fox jumps over a lazy dog.");

    Option opt2_entry("bar", '\0', "help doc group 2",
                      &opt2, Option::FLAG_NONE,
                      "string option bar", "STR");

    Option opt3_entry("baz", 'b', "group2",
                      &opt3, Option::FLAG_NONE, ValueLimit::Fixed(3),
                      "int list option baz", "INT INT INT");

    OptionParser parser;
    parser.add_option(opt1_entry);
    parser.add_option(opt2_entry);
    parser.add_option(opt3_entry);
    parser.parse_options(argc, argv);

    cout << parser.format_document() << endl;
    cout << "after parse" << endl
         << "  opt1: " << opt1 << endl
         << "  opt2: " << opt2 << endl
         << "  opt3: ";
    for (size_t i=0; i< opt3.size(); i++)
    {
        cout << opt3[i] << " ";
    }
    cout << endl;

    cout << "remaining options: " << argc << endl;
    for (int i=0; i<argc; i++)
    {
        cout << "  " << argv[i] << endl;
    }
}



