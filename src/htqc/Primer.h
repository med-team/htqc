#ifndef PRIMER_H
#define PRIMER_H

#include "htio2/Kmer.h"

namespace htqc
{

typedef uint32_t KmerT;
typedef int16_t PosT;
typedef std::vector<KmerT> KmerList;
typedef std::multimap<KmerT, PosT> KmerPosMap;

struct Primer
{
    Primer(const std::string& id, const std::string& seq, size_t k);

    size_t length() const { return seq_fwd.length(); }
    bool align_fwd(const std::string& seq, const std::vector<int16_t> qual, const KmerList& seq_kmer,
                   int cut_qual, int cut_mis_high, int cut_mis_total,
                   PosT& seq_start, PosT& seq_end,
                   PosT& primer_start, PosT& primer_end) const;
    bool align_rev(const std::string& seq, const std::vector<int16_t> qual, const KmerList& seq_kmer,
                   int cut_qual, int cut_mis_high, int cut_mis_total,
                   PosT& seq_start, PosT& seq_end,
                   PosT& primer_start, PosT& primer_end) const;

    std::string id;
    std::string seq_fwd;
    std::string seq_rev;
    KmerList klist_fwd;
    KmerList klist_rev;
    KmerPosMap kpos_fwd;
    KmerPosMap kpos_rev;
};

struct PrimerGroup
{
    PrimerGroup(const std::string& name);
    std::string name;
    std::vector<Primer> primers_left;
    std::vector<Primer> primers_right;
};

} // namespace htqc

#endif // PRIMER_H
