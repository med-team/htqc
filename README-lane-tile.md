ht2-lane-tile - remove reads from specific lane/tile
====================================================

Introduction
------------

Tiles are rectangular areas in an Illumina sequencing chip. Sometimes, specific
tiles may produce very few amount of acceptable reads, and some people think it
is better to reject all reads from those tiles. Thus we provide this program to
reject reads from specific tiles.

Input and Output
----------------

This program do not distinguish single-end and paired-end inputs. Paired-end
inputs can be processed by apply the program on each file.

Usage
-----

Reject sequences from tile 2 and 3:

    $ ht2-lane-tile -T 2 3 -i input.fastq -o output

If your input file contains reads from multiple lanes, and you only wish to
reject sequences from a specific lane-tile, you can specify lane number. For
example, you want to reject tile 2 and 3 in lane 5:

    $ ht2-lane-tile -L 5 -T 2 3 -i input.fastq -o output

If you also want to get those rejected reads, specify the output prefix with
`-u` option:

    $ ht2-lane-tile -L 5 -T 2 3 -i input.fastq -o output -u rejected
