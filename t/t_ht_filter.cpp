#include "TestFramework.h"
#include "TestConfig.h"

#include "htio2/FastqIO.h"
#include "htio2/FastqSeq.h"

#define BIN "../src/ht2-filter"
#define FILE1 TEST_SOURCE_DIR "/test1.fastq.gz"
#define FILE2 TEST_SOURCE_DIR "/test2.fastq.gz"

#include "htio2/JUCE-3.0.8/JuceHeader.h"

using namespace std;
using namespace htio2::juce;

size_t high_qual_len(const string& qual)
{
    size_t num = 0;

    for (size_t i = 0; i < qual.length(); i++)
    {
        // 33-based, quality cutoff 30
        if (qual[i] >= 33 + 30)
            num++;
    }

    return num;
}

void TestFramework::content()
{
    // paired-end test
    {
        const char* cmd =
                BIN
                " -q -P"
                " -Q 30 -L 120"
                " -i " FILE1 " " FILE2
                " -o filt"
                " -u filt_fail";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/filt_1.fastq.gz", "filt_1.fastq.gz"));
        OK(text_file_eq(TEST_SOURCE_DIR "/filt_2.fastq.gz", "filt_2.fastq.gz"));
        OK(text_file_eq(TEST_SOURCE_DIR "/filt_s.fastq.gz", "filt_s.fastq.gz"));
        // ID match
        OK(seq_file_id_eq("filt_1.fastq.gz", "filt_2.fastq.gz"));
        OK(seq_file_id_eq("filt_fail_1.fastq.gz", "filt_fail_2.fastq.gz"));
        OK(seq_file_id_eq("filt_s.fastq.gz", "filt_fail_s.fastq.gz"));
    }

    {
        const char* cmd =
                BIN
                " -q -S"
                " -Q 30 -L 120"
                " -i " TEST_SOURCE_DIR "/test_interleave.fastq.gz"
                " -o filt -u filt_fail";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/filt.fastq.gz", "filt.fastq.gz"));
        OK(text_file_eq(TEST_SOURCE_DIR "/filt_fail.fastq.gz", "filt_fail.fastq.gz"));
    }

    {
        vector<string> files_accepted;
        files_accepted.push_back("filt_1.fastq.gz");
        files_accepted.push_back("filt_2.fastq.gz");
        files_accepted.push_back("filt_s.fastq.gz");
        files_accepted.push_back("filt.fastq.gz");

        vector<string> files_rejected;
        files_rejected.push_back("filt_fail_1.fastq.gz");
        files_rejected.push_back("filt_fail_2.fastq.gz");
        files_rejected.push_back("filt_fail_s.fastq.gz");
        files_rejected.push_back("filt_fail.fastq.gz");

        size_t num = 0;
        htio2::FastqSeq seq;

        for (size_t i = 0; i < files_accepted.size(); i++)
        {
            htio2::FastqIO fh(files_accepted[i], htio2::READ, htio2::COMPRESS_GZIP);
            while (fh.next_seq(seq))
            {
                GE(high_qual_len(seq.quality), 120);
                num++;
            }
        }

        for (size_t i = 0; i < files_rejected.size(); i++)
        {
            htio2::FastqIO fh(files_rejected[i], htio2::READ, htio2::COMPRESS_GZIP);
            while (fh.next_seq(seq))
            {
                LT(high_qual_len(seq.quality), 120);
                num++;
            }
        }

        is(num, 400, "400 sequences in total");
    }
}
