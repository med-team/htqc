ht2-primer-trim - find and remove primers from reads
===================================================

Introduction
------------

`ht2-primer-trim` removes primer sequences from reads. This task is common for
sequencing on PCR-amplified products, such as 16S, ITS, and other phylogenetic
marker sequences.

Input and Output
----------------

The program accepts single- and paired-end input reads. The primer sequences
should be given in one FASTA file. The ID of 5' primers should begin with
"left", and 3' ones begin with "right". Output targets should be given by
description. Multiplexed primers are accepted.

Only the reads with primers are trimmed and written to output. User
can specify a file prefix for those reads failed to found primers.

For example:

    >left1 16S
    XXXXXXXXXXXXXXX
    >left2 16S
    XXXXXXXXXXXXXXX
    >left3 ITS
    XXXXXXXXXXXXXXX
    >left4 ITS
    XXXXXXXXXXXXXXX
    >right1 16S
    XXXXXXXXXXXXXXX
    >right2 ITS
    XXXXXXXXXXXXXXX

  - if a sequence matches with left1 and right1, or left2 and right1, it will be
    written to `OUT_DIR/16S.fastq`;

  - if a sequences matches with left3 and right2, or left4 and right2, it will
    be written to `OUT/ITS.fastq`;

  - if a sequence matches with left1 and right2, or only match with one primer,
    or don't match with any primers, it will be discarded, or written to the
    file for failed sequences (if specified by user).

Primer Matching Strategy
------------------------

When processing single-end reads, primers are attempted to be aligned in
pattern below:

    >>>> left primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                            <<<< right primer <<<<

If reverse-complement check is specified, the primers are
also to be aligned in the pattern below:

    >>>> right primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                             <<<< left primer <<<<

When processing paired-end reads, primers are attempted to be aligned in
pattern below:

    >>>> left primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read 1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    >>>> right primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

If the reverse-complement check is specified, the primers are
also to be aligned in the pattern below:

    >>>> right primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read 1 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    >>>> left primer >>>>
    >>>>>>>>>>>>>>>>>>>>>>>>>> read 2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
