#ifndef HTIO2_VERSION_H
#define	HTIO2_VERSION_H

#include <string>

namespace htio2
{

/**
 * get the version of HTIO library
 * @return version string formatted in "MAJOR.MINOR.PATCH"
 */
std::string get_version_string();

} // namespace htio2

#endif	/* HTIO2_VERSION_H */

