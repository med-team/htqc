#include "htqc/App.h"
#include "htio2/Cast.h"

using namespace htqc;
using namespace std;

int main(int argc, char** argv)
{
    // parse and validate options
    parse_options(argc, argv);
    validate_options();

    // guess compression by input files
    OPT_compress = guess_compress_format_by_files_name(OPT_files_in);
    if (OPT_compress == htio2::COMPRESS_UNKNOWN)
    {
        cerr << "ERROR: failed to guess compress format from input files" << endl;
        exit(EXIT_FAILURE);
    }

    // show options
    if (!OPT_quiet)
    {
        std::cout << std::endl
                  << "#---------------------------------#" << std::endl
                  << "#" << std::endl
                  << "# Options:" << std::endl
                  << "#" << std::endl;
        show_options();
        std::cout << "# compress: " << htio2::to_string(OPT_compress) << std::endl
                  << "#" << std::endl
                  << "#---------------------------------#" << std::endl
                  << std::endl;
    }

    // run
    if (!OPT_quiet) std::cout << "start processing" << std::endl;
    execute();
    if (!OPT_quiet) std::cout << "done" << std::endl;
}

