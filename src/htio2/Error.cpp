#include "htio2/Error.h"
#include <sstream>
#include "htio2/Cast.h"

namespace htio2
{

InvalidQualityEncode::InvalidQualityEncode(QualityEncode encode)
: std::invalid_argument(to_string(encode))
{ }

InvalidQualityEncode::~InvalidQualityEncode() throw ()
{ }

InvalidHeaderFormat::InvalidHeaderFormat(HeaderFormat format)
: std::invalid_argument(to_string(format))
{ }

InvalidHeaderFormat::~InvalidHeaderFormat() throw ()
{ }

InvalidCompressFormat::InvalidCompressFormat(CompressFormat comp)
: std::invalid_argument(to_string(comp))
{ }

InvalidCompressFormat::~InvalidCompressFormat() throw ()
{ }

InvalidSeqFormat::InvalidSeqFormat(SeqFormat format)
: std::invalid_argument(to_string(format))
{ }

InvalidSeqFormat::~InvalidSeqFormat() throw ()
{ }

InvalidRevcom::InvalidRevcom(const std::string& seq)
: std::invalid_argument(seq)
{ }

InvalidRevcom::~InvalidRevcom() throw ()
{ }

std::string _format_file_message(const std::string& content, size_t offset)
{
    std::ostringstream buffer;
    buffer << "invalid file content at offset " << offset << ": " << content;
    return buffer.str();
}

InvalidFileContent::InvalidFileContent(const std::string& content, size_t offset)
: std::runtime_error(_format_file_message(content, offset))
{ }

InvalidFileContent::~InvalidFileContent() throw ()
{ }

std::string _format_kmer_message_(size_t size, size_t limit)
{
    std::ostringstream buffer;
    buffer << "kmer size " << size << " exceeds limit " << limit;
    return buffer.str();
}

InvalidKmerSize::InvalidKmerSize(size_t size, size_t limit)
    : std::runtime_error(_format_kmer_message_(size, limit))
{ }

InvalidKmerSize::~InvalidKmerSize() throw()
{ }

} // namespace htio2
