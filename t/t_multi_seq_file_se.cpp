#include "TestFramework.h"
#include "TestConfig.h"
#include "htqc/MultiSeqFile.h"

using namespace std;

void TestFramework::content()
{
    vector<string> files;
    files.push_back(TEST_SOURCE_DIR"/test1.fastq");
    files.push_back(TEST_SOURCE_DIR"/test2.fastq");
    files.push_back(TEST_SOURCE_DIR"/test2.fastq");
    files.push_back(TEST_SOURCE_DIR"/test1.fastq");
    htqc::MultiSeqFileSE fh(files, htio2::COMPRESS_PLAIN);

    htio2::FastqSeq seq;
    size_t n_seq = 0;
    while (fh.next_seq(seq))
    {
        n_seq++;
        if (n_seq==1)
        {
            IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
            IS(seq.desc, "1:N:0:CTTGTA");
        }
        else if (n_seq == 101)
        {
            IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
            IS(seq.desc, "2:N:0:CTTGTA");
        }
        else if (n_seq == 201)
        {
            IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
            IS(seq.desc, "2:N:0:CTTGTA");
        }
        else if (n_seq == 301)
        {
            IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
            IS(seq.desc, "1:N:0:CTTGTA");
        }
        else if (n_seq == 302)
        {
            IS(seq.id, "HWI-ST1106:815:H154GADXX:1:1101:1197:2208");
            IS(seq.desc, "1:N:0:TTGGTA");
        }
    }

    IS(n_seq, 400);
}



