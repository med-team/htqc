#ifndef HTQC_APP_H
#define	HTQC_APP_H

#include <iostream>

#include "htqc/Options.h"

void parse_options(int argc, char** argv);
void validate_options();
void show_options();
void execute();

#endif	/* HTQC_APP_H */

