HTQC installation guide
=======================

CMake
-----

HTQC uses CMake to generate platform-specific build files. You must have CMake
to build HTQC. To test whether you have CMake on your system, run:
  $ cmake -h

It should print help information if CMake has been properly installed.

If you don't have a CMake in your system, please use your Unix system's package
management system, or download CMake from their website: http://www.cmake.org/

Simple install
--------------

Goto the source code directory:

    $ cd (where_you_put_the_code)/htqc-X.X.X-Source

In the source code dir, create a place to conatin build results:

    $ mkdir build
    $ cd build

* Now we are in (where_you_put_the_code)/htqc-X.X.X-Source/build.
* Please make sure the file "CMakeLists.txt" exists in the upper directory of
  "build" directory.

Run cmake to generate makefile:

    $ cmake ..

* ".." indicates the upper directory is the root of project
* the file "Makefile" should be generated in current directory

Build and install

    $ make
    $ su
    $ make install

Customized install
------------------

If you can't get system-wide permission to copy programs to "/usr/local/bin",
you can install the program only for yourself. To do this, run cmake by
specifying install prefix to a directory under your control:

    $ cmake -DCMAKE_INSTALL_PREFIX=~ ../
    $ make
    $ make install

It will install the programs to "~/bin" and libraries to "~/lib", which you
have permissions to create files.

For more options, please refer to the handbook of CMake.
