//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTIO2_FILE_HANDLE_H
#define	HTIO2_FILE_HANDLE_H

#include <string>

#include "htio2/RefCounted.h"
#include "htio2/Enum.h"

namespace htio2
{

#define LINE_END_CR 13
#define LINE_END_LF 10

inline const char* get_io_mode_str(IOMode mode)
{
    switch (mode)
    {
    case READ:
        return "rb";
    case WRITE:
        return "wb";
    case APPEND:
        return "ab";
    default:
        return 0;
    }
}

/**
 * An abstract base class for file handles.
 */
class FileHandle : public RefCounted
{
public:
    typedef SmartPtr<FileHandle> Ptr;
    typedef SmartPtr<const FileHandle> ConstPtr;

public:
    FileHandle();
    virtual ~FileHandle();

    /**
     * Read one line. The end-of-line character can be CR, LF and CRLF.
     * @param buffer
     * @return true if get a line, false if reached end of file.
     */
    virtual bool read_line(std::string& buffer) = 0;

    /**
     * Write one line. The end-of-line character is decided by the system.
     * @param content the content to be written
     */
    virtual void write_line(const std::string& content) = 0;

    /**
     * Seek to a file offset.
     * The offset can be different with the wrapped FILE* or gzFile's offset.
     * See implementation of read_line() for detail.
     * @param offset
     * @param whence 0 for absolute, 1 for relative
     */
    virtual void seek(off_t offset, int whence = SEEK_SET) = 0;

    /**
     * Get the current file offset.
     * The offset can be different with the wrapped FILE* or gzFile's offset.
     * See implementation of read_line() for detail.
     * @return offset
     */
    virtual off_t tell() const = 0;

private:
};

} // namespace htio2

#endif	/* HTIO2_FILE_HANDLE_H */

