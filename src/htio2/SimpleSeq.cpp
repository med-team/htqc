/*
 * File:   FastaSeq.cpp
 * Author: yangxi
 *
 * Created on December 25, 2012, 3:15 PM
 */

#include "htio2/SimpleSeq.h"
#include "Error.h"

namespace htio2
{

void revcom(const std::string& in, std::string& out)
{
    out.clear();
    out.reserve(in.size());
    for (int i = in.size() - 1; i >= 0; i--)
    {
        char comp = 0;
        switch (in[i])
        {
        case 'A':
            comp = 'T';
            break;
        case 'a':
            comp = 't';
            break;
        case 'T':
            comp = 'A';
            break;
        case 't':
            comp = 'a';
            break;
        case 'G':
            comp = 'C';
            break;
        case 'g':
            comp = 'c';
            break;
        case 'C':
            comp = 'G';
            break;
        case 'c':
            comp = 'g';
            break;
        case 'N':
            comp = 'N';
            break;
        case 'n':
            comp = 'n';
            break;
        }
        if (!comp)
            throw InvalidRevcom(in);
        out.push_back(comp);
    }
}

SimpleSeq::SimpleSeq()
{
}

SimpleSeq::SimpleSeq(const std::string& id, const std::string& desc, const std::string& seq) :
id(id),
desc(desc),
seq(seq)
{
}

SimpleSeq::~SimpleSeq()
{
}

void SimpleSeq::revcom(SimpleSeq& result) const
{
    result.id = id;
    result.desc = desc;
    htio2::revcom(seq, result.seq);
}

void SimpleSeq::subseq(SimpleSeq& result, size_t start, size_t length) const
{
    result.id = id;
    result.desc = desc;
    result.seq.assign(seq, start, length);
}

} // namespace htio2
