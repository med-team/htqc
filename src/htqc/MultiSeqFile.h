#ifndef HTQC_MULTISEQFILEINPUT_H
#define	HTQC_MULTISEQFILEINPUT_H

#include "htio2/PairedEndIO.h"
#include "htio2/SeqIO.h"

#include <vector>

namespace htqc
{

class MultiSeqFileSE: public htio2::RefCounted
{
public:
    typedef htio2::SmartPtr<MultiSeqFileSE> Ptr;
    typedef htio2::SmartPtr<const MultiSeqFileSE> ConstPtr;

public:
    MultiSeqFileSE(const std::vector<std::string>& files, htio2::CompressFormat comp);
    virtual ~MultiSeqFileSE();
    bool next_seq(htio2::FastqSeq& seq);

protected:
    std::vector<std::string> files;
    htio2::SeqIO::Ptr fh;
    htio2::CompressFormat comp;
    size_t curr_file_i;
private:
    MultiSeqFileSE(const MultiSeqFileSE& other);
    MultiSeqFileSE& operator=(const MultiSeqFileSE& other);
};

class MultiSeqFilePE: public htio2::RefCounted
{
public:
    typedef htio2::SmartPtr<MultiSeqFilePE> Ptr;
    typedef htio2::SmartPtr<const MultiSeqFilePE> ConstPtr;

public:
    MultiSeqFilePE(const std::vector<std::string>& files,
                   htio2::Strand strand_r1_in_file,
                   htio2::Strand strand_r2_in_file,
                   htio2::CompressFormat comp);

    MultiSeqFilePE(const std::vector<std::string>& files_a,
                   const std::vector<std::string>& files_b,
                   htio2::Strand strand_r1_in_file,
                   htio2::Strand strand_r2_in_file,
                   htio2::CompressFormat comp);
    virtual ~MultiSeqFilePE();

    bool next_pair(htio2::FastqSeq& seq1,
                   htio2::FastqSeq& seq2,
                   htio2::Strand strand_r1,
                   htio2::Strand strand_r2);

protected:
    std::vector<std::string> files_a;
    std::vector<std::string> files_b;
    size_t curr_file_i;
    htio2::PairedEndIO* fh;

    bool interleave;
    htio2::Strand strand_r1_in_file;
    htio2::Strand strand_r2_in_file;
    htio2::CompressFormat comp;
private:
    MultiSeqFilePE(const MultiSeqFilePE& other);
    MultiSeqFilePE& operator=(const MultiSeqFilePE& other);
};

} // namespace htqc

#endif	/* HTQC_MULTISEQFILEINPUT_H */

