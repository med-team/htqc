#include "TestFramework.h"
#include "TestConfig.h"
#include "htqc/MultiSeqFile.h"

using namespace std;

void TestFramework::content()
{
    {
        OK("test file pairs");
        vector<string> files1;
        files1.push_back(TEST_SOURCE_DIR"/test1.fastq.gz");
        files1.push_back(TEST_SOURCE_DIR"/test1.fastq.gz");
        vector<string> files2;
        files2.push_back(TEST_SOURCE_DIR"/test2.fastq.gz");
        files2.push_back(TEST_SOURCE_DIR"/test2.fastq.gz");

        htqc::MultiSeqFilePE fh(files1, files2, htio2::STRAND_FWD, htio2::STRAND_REV, htio2::COMPRESS_GZIP);

        htio2::FastqSeq seq1;
        htio2::FastqSeq seq2;

        size_t count = 0;

        while (fh.next_pair(seq1, seq2, htio2::STRAND_FWD, htio2::STRAND_REV))
        {
            count++;
            if (count == 1)
            {
                IS(seq1.id,      "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq1.desc,    "1:N:0:CTTGTA");
                IS(seq1.seq,     "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
                IS(seq1.quality, "=\?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3\?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA\?ABCAC@9@AA8\?\?8><ADB<>B8<B:C\?>@>>34>:@CD");
                IS(seq2.id,      "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq2.desc,    "2:N:0:CTTGTA");
                IS(seq2.seq,     "CCATTACCGCGGCTGCTGGCACGGAGTTAGCCGATGCTTATTCATAAGGTACATACAAGCTCCCACACGTGGGAGGGTTTATTCCCTTATAAAAGAAGTTTACAATCCGTAGGACCTTCATCCTTCACGCTACTTGGCTGGTTCAGACTC");
                IS(seq2.quality, "=+=+<\?7@\?C0CACAABABABBAA=:<AABBBAAAAABBAABBBBAAAAA;\?@>>75>>=@>\?======;9;=8255+88=:\?\?\?\?\?\?\?\?\?=::=;<\?8883::8;8;;=;<=\?\?7<\?\?\?\?3;\?\?\?\?\?8;===\?\?\?\?\?\?###########");
            }
            else if (count==100)
            {
                IS(seq1.id,      "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq2.id,      "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq1.desc,    "1:N:0:TTAGGC");
                IS(seq2.desc,    "2:N:0:TTAGGC");
                IS(seq1.seq,     "ATCGCAAATCATGGTGGGGATGATACGGCGTTTCATTGCGGAGCAGGAAATTGTTATGTTCTTATCAGGAGATTATAATTGGCCTGACAAATGGATGTGATTTATTTATTGGGCTCAATTGGGCTAGGTTGGGGATCTTCATTTGTCCAC");
                IS(seq2.seq,     "ATGCTACCTACTCTGAAATTGACGGTACAGCAACAAGCGAAATTGCGACAAGCAGTAATGCATTAATGTTAATCCCTCAGAATGATCTATCGTCAGTCAAGATTACTGTTACTTATTCCACGACTAATGATAATGGCAAAGTATTTGAAG");
                IS(seq1.quality, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
                IS(seq2.quality, "\?==BBDFFGFHHGGIIIIFDCFC@FGHIIIIIIGIIIGHHGIFIIHIIIIIIGIIIGHFHHHGFFFFEEEEEEECECCDDDDDCDDCDCCCB\?ABDDDCDDDDACCDA>CCCDDDCEAA@ABBBB<>A>CCDCCDCBC><CAABC@@:@:");
            }
            else if (count==101)
            {
                IS(seq1.id,      "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq2.id,      "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq1.desc,    "1:N:0:CTTGTA");
                IS(seq2.desc,    "2:N:0:CTTGTA");
                IS(seq1.seq,     "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
                IS(seq2.seq,     "CCATTACCGCGGCTGCTGGCACGGAGTTAGCCGATGCTTATTCATAAGGTACATACAAGCTCCCACACGTGGGAGGGTTTATTCCCTTATAAAAGAAGTTTACAATCCGTAGGACCTTCATCCTTCACGCTACTTGGCTGGTTCAGACTC");
                IS(seq1.quality, "=\?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3\?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA\?ABCAC@9@AA8\?\?8><ADB<>B8<B:C\?>@>>34>:@CD");
                IS(seq2.quality, "=+=+<\?7@\?C0CACAABABABBAA=:<AABBBAAAAABBAABBBBAAAAA;\?@>>75>>=@>\?======;9;=8255+88=:\?\?\?\?\?\?\?\?\?=::=;<\?8883::8;8;;=;<=\?\?7<\?\?\?\?3;\?\?\?\?\?8;===\?\?\?\?\?\?###########");
            }
            else if (count==200)
            {
                IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq1.desc, "1:N:0:TTAGGC");
                IS(seq2.desc, "2:N:0:TTAGGC");
                IS(seq1.seq, "ATCGCAAATCATGGTGGGGATGATACGGCGTTTCATTGCGGAGCAGGAAATTGTTATGTTCTTATCAGGAGATTATAATTGGCCTGACAAATGGATGTGATTTATTTATTGGGCTCAATTGGGCTAGGTTGGGGATCTTCATTTGTCCAC");
                IS(seq2.seq, "ATGCTACCTACTCTGAAATTGACGGTACAGCAACAAGCGAAATTGCGACAAGCAGTAATGCATTAATGTTAATCCCTCAGAATGATCTATCGTCAGTCAAGATTACTGTTACTTATTCCACGACTAATGATAATGGCAAAGTATTTGAAG");
                IS(seq1.quality, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
                IS(seq2.quality, "\?==BBDFFGFHHGGIIIIFDCFC@FGHIIIIIIGIIIGHHGIFIIHIIIIIIGIIIGHFHHHGFFFFEEEEEEECECCDDDDDCDDCDCCCB\?ABDDDCDDDDACCDA>CCCDDDCEAA@ABBBB<>A>CCDCCDCBC><CAABC@@:@:");
            }
        }

        IS(count, 200);
    }

    {
        OK("test interleaved files");
        vector<string> files;
        files.push_back(TEST_SOURCE_DIR"/test_interleave.fastq.gz");
        files.push_back(TEST_SOURCE_DIR"/test_interleave.fastq.gz");

        htqc::MultiSeqFilePE fh(files, htio2::STRAND_FWD, htio2::STRAND_REV, htio2::COMPRESS_GZIP);

        htio2::FastqSeq seq1;
        htio2::FastqSeq seq2;

        size_t count = 0;

        while (fh.next_pair(seq1, seq2, htio2::STRAND_FWD, htio2::STRAND_REV))
        {
            count++;
            if (count == 1)
            {
                IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq1.desc, "1:N:0:CTTGTA");
                IS(seq1.seq,     "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
                IS(seq1.quality, "=\?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3\?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA\?ABCAC@9@AA8\?\?8><ADB<>B8<B:C\?>@>>34>:@CD");
                IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq2.desc, "2:N:0:CTTGTA");
                IS(seq2.seq,     "CCATTACCGCGGCTGCTGGCACGGAGTTAGCCGATGCTTATTCATAAGGTACATACAAGCTCCCACACGTGGGAGGGTTTATTCCCTTATAAAAGAAGTTTACAATCCGTAGGACCTTCATCCTTCACGCTACTTGGCTGGTTCAGACTC");
                IS(seq2.quality, "=+=+<\?7@\?C0CACAABABABBAA=:<AABBBAAAAABBAABBBBAAAAA;\?@>>75>>=@>\?======;9;=8255+88=:\?\?\?\?\?\?\?\?\?=::=;<\?8883::8;8;;=;<=\?\?7<\?\?\?\?3;\?\?\?\?\?8;===\?\?\?\?\?\?###########");
            }
            else if (count==100)
            {
                IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq1.desc, "1:N:0:TTAGGC");
                IS(seq1.seq, "ATCGCAAATCATGGTGGGGATGATACGGCGTTTCATTGCGGAGCAGGAAATTGTTATGTTCTTATCAGGAGATTATAATTGGCCTGACAAATGGATGTGATTTATTTATTGGGCTCAATTGGGCTAGGTTGGGGATCTTCATTTGTCCAC");
                IS(seq1.quality, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
                IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq2.desc, "2:N:0:TTAGGC");
                IS(seq2.seq, "ATGCTACCTACTCTGAAATTGACGGTACAGCAACAAGCGAAATTGCGACAAGCAGTAATGCATTAATGTTAATCCCTCAGAATGATCTATCGTCAGTCAAGATTACTGTTACTTATTCCACGACTAATGATAATGGCAAAGTATTTGAAG");
                IS(seq2.quality, "\?==BBDFFGFHHGGIIIIFDCFC@FGHIIIIIIGIIIGHHGIFIIHIIIIIIGIIIGHFHHHGFFFFEEEEEEECECCDDDDDCDDCDCCCB\?ABDDDCDDDDACCDA>CCCDDDCEAA@ABBBB<>A>CCDCCDCBC><CAABC@@:@:");
            }
            else if (count==101)
            {
                IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq1.desc, "1:N:0:CTTGTA");
                IS(seq1.seq,     "AGCCTACGGGAGGCAGCAGTGAGGAATATTGGTCAATGGACGAGAGTCTGAACCAGCCAAGTAGCGTGAAGGATGAAGGTCCTACGGATTGTAAACTTCTTTTATAAGGGAATAAACCCTCCCACGTGTGGGAGCTTGTATGTACCTTAT");
                IS(seq1.quality, "=\?@DFADDHHAHDGGBGGGIGHBEGGHEI>FGIIGCGGCH3\?@FEB@@@FGICHG==AHFFEFEEDDDDDDC<>A>A:ACDCC>C<@BB>C@ACC@CACAC@ACCDDDCA\?ABCAC@9@AA8\?\?8><ADB<>B8<B:C\?>@>>34>:@CD");
                IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:1244:2196");
                IS(seq2.desc, "2:N:0:CTTGTA");
                IS(seq2.seq,     "CCATTACCGCGGCTGCTGGCACGGAGTTAGCCGATGCTTATTCATAAGGTACATACAAGCTCCCACACGTGGGAGGGTTTATTCCCTTATAAAAGAAGTTTACAATCCGTAGGACCTTCATCCTTCACGCTACTTGGCTGGTTCAGACTC");
                IS(seq2.quality, "=+=+<\?7@\?C0CACAABABABBAA=:<AABBBAAAAABBAABBBBAAAAA;\?@>>75>>=@>\?======;9;=8255+88=:\?\?\?\?\?\?\?\?\?=::=;<\?8883::8;8;;=;<=\?\?7<\?\?\?\?3;\?\?\?\?\?8;===\?\?\?\?\?\?###########");
            }
            else if (count==200)
            {
                IS(seq1.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq1.desc, "1:N:0:TTAGGC");
                IS(seq1.seq, "ATCGCAAATCATGGTGGGGATGATACGGCGTTTCATTGCGGAGCAGGAAATTGTTATGTTCTTATCAGGAGATTATAATTGGCCTGACAAATGGATGTGATTTATTTATTGGGCTCAATTGGGCTAGGTTGGGGATCTTCATTTGTCCAC");
                IS(seq1.quality, "@<BFFFFFHGHHHJCFGIIFIJIGJJJJJJIJJGIIJJJJJJHFFFFFCCEDECDDEDFEEEDCDCCDDABDDCDDEEEEDDDDDBCDDDDDDD(:@AACDDDDEDDDEDDDDBDAACCDCDDDDBDDCDDDDD0<BDCDEEEEEE@CDD");
                IS(seq2.id, "HWI-ST1106:815:H154GADXX:1:1101:3431:2221");
                IS(seq2.desc, "2:N:0:TTAGGC");
                IS(seq2.seq, "ATGCTACCTACTCTGAAATTGACGGTACAGCAACAAGCGAAATTGCGACAAGCAGTAATGCATTAATGTTAATCCCTCAGAATGATCTATCGTCAGTCAAGATTACTGTTACTTATTCCACGACTAATGATAATGGCAAAGTATTTGAAG");
                IS(seq2.quality, "\?==BBDFFGFHHGGIIIIFDCFC@FGHIIIIIIGIIIGHHGIFIIHIIIIIIGIIIGHFHHHGFFFFEEEEEEECECCDDDDDCDDCDCCCB\?ABDDDCDDDDACCDA>CCCDDDCEAA@ABBBB<>A>CCDCCDCBC><CAABC@@:@:");
            }
        }

        IS(count, 200);
    }
}
