ht2-trim - remove bad residues from sequence start/end
=====================================================

Introduction
------------

The quality of sequencing reads from Illumina platform usually drops along
length. ht2-trim can remove low-quality part from the start and the end of reads.

The trim stops at the first *N* continuous bases in which qualities are all higher
than *Q*. (N=5 and Q=20 by default).

Input and output
----------------

The program don't distinguish single-end and paired-end inputs. If you need to
process paired-end reads stored in separate file pairs, just run the program on
each file.

Usage
-----

Trim from both side:

    $ ht2-trim -i in.fastq -o out.fastq

Only trim from end, keep start:

    $ ht2-trim --trim-side head -i in.fastq -o out.fastq
