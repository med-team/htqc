//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "htio2/FastqSeq.h"

using namespace std;

namespace htio2
{

FastqSeq::FastqSeq()
{
}

FastqSeq::~FastqSeq()
{
}

void FastqSeq::revcom(FastqSeq& result) const
{
    SimpleSeq::revcom(result);
    result.quality.assign(quality.rbegin(), quality.rend());
}

void FastqSeq::subseq(FastqSeq& result, size_t start, size_t length) const
{
    SimpleSeq::subseq(result, start, length);
    result.quality.assign(quality, start, length);
}

} // namespace htio2
