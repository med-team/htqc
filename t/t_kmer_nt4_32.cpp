#include "TestFramework.h"
#include "htio2/Kmer.h"

#include <limits>

using namespace htio2;
using namespace std;

typedef uint32_t KmerType;

void TestFramework::content()
{
    // encode
    EncodedSeq enc;
    encode_nt4("ATGXCATAATGXCATA", enc);
    IS(enc.size(), 16);
    IS(enc[0], ENCODED_NT4_A);
    IS(enc[1], ENCODED_NT4_T);
    IS(enc[2], ENCODED_NT4_G);
    IS(enc[3], ENCODED_ERROR);
    IS(enc[4], ENCODED_NT4_C);
    IS(enc[5], ENCODED_NT4_A);
    IS(enc[6], ENCODED_NT4_T);
    IS(enc[7], ENCODED_NT4_A);
    IS(enc[8], ENCODED_NT4_A);
    IS(enc[9], ENCODED_NT4_T);
    IS(enc[10], ENCODED_NT4_G);
    IS(enc[11], ENCODED_ERROR);
    IS(enc[12], ENCODED_NT4_C);
    IS(enc[13], ENCODED_NT4_A);
    IS(enc[14], ENCODED_NT4_T);
    IS(enc[15], ENCODED_NT4_A);

    {
        // generate kmer
        vector<KmerType> kmers;
        OK(gen_kmer_nt4(enc, kmers, 3));
        IS(kmers.size(), 14);
        IS(kmers[0], ENCODED_NT4_A + ENCODED_NT4_T*4 + ENCODED_NT4_G*4*4);
        IS(kmers[1], numeric_limits<KmerType>::max());
        IS(kmers[2], numeric_limits<KmerType>::max());
        IS(kmers[3], numeric_limits<KmerType>::max());
        IS(kmers[4], ENCODED_NT4_C + ENCODED_NT4_A*4 + ENCODED_NT4_T*4*4);
        IS(kmers[5], ENCODED_NT4_A + ENCODED_NT4_T*4 + ENCODED_NT4_A*4*4);
        IS(kmers[6], ENCODED_NT4_T + ENCODED_NT4_A*4 + ENCODED_NT4_A*4*4);
        IS(kmers[7], ENCODED_NT4_A + ENCODED_NT4_A*4 + ENCODED_NT4_T*4*4);
        IS(kmers[8], ENCODED_NT4_A + ENCODED_NT4_T*4 + ENCODED_NT4_G*4*4);
        IS(kmers[9], numeric_limits<KmerType>::max());
        IS(kmers[10], numeric_limits<KmerType>::max());
        IS(kmers[11], numeric_limits<KmerType>::max());
        IS(kmers[12], ENCODED_NT4_C + ENCODED_NT4_A*4 + ENCODED_NT4_T*4*4);
        IS(kmers[13], ENCODED_NT4_A + ENCODED_NT4_T*4 + ENCODED_NT4_A*4*4);

        // decode kmer
        IS(decode_kmer_nt4(kmers[0], 3), "ATG");
        IS(decode_kmer_nt4(kmers[1], 3), "");
        IS(decode_kmer_nt4(kmers[2], 3), "");
        IS(decode_kmer_nt4(kmers[3], 3), "");
        IS(decode_kmer_nt4(kmers[4], 3), "CAT");
        IS(decode_kmer_nt4(kmers[5], 3), "ATA");
        IS(decode_kmer_nt4(kmers[6], 3), "TAA");
        IS(decode_kmer_nt4(kmers[7], 3), "AAT");
        IS(decode_kmer_nt4(kmers[8], 3), "ATG");
        IS(decode_kmer_nt4(kmers[9], 3), "");
        IS(decode_kmer_nt4(kmers[10], 3), "");
        IS(decode_kmer_nt4(kmers[11], 3), "");
        IS(decode_kmer_nt4(kmers[12], 3), "CAT");
        IS(decode_kmer_nt4(kmers[13], 3), "ATA");
    }

    {
        vector<KmerType> kmers;
        OK(gen_kmer_nt4(enc, kmers, 15));
        IS(kmers.size(), 2);

        KmerType kmer =
                ENCODED_NT4_A +
                ENCODED_NT4_T *4 +
                ENCODED_NT4_G *4*4 +
                ENCODED_NT4_C *4*4*4 +
                ENCODED_NT4_A *4*4*4*4 +
                ENCODED_NT4_T *4*4*4*4*4 +
                ENCODED_NT4_A *4*4*4*4*4*4 +
                ENCODED_NT4_A *4*4*4*4*4*4*4 +
                ENCODED_NT4_T *4*4*4*4*4*4*4*4 +
                ENCODED_NT4_G *4*4*4*4*4*4*4*4*4 +
                ENCODED_NT4_C *4*4*4*4*4*4*4*4*4*4 +
                ENCODED_NT4_A *4*4*4*4*4*4*4*4*4*4*4 +
                ENCODED_NT4_T *4*4*4*4*4*4*4*4*4*4*4*4 +
                ENCODED_NT4_A *4*4*4*4*4*4*4*4*4*4*4*4*4 +
                ENCODED_NT4_A *4*4*4*4*4*4*4*4*4*4*4*4*4*4;
        IS(gen_kmer_nt4<KmerType>("ATGCATAATGCATAA"), kmer);
    }

    {
        vector<KmerType> kmers;
        OK(!gen_kmer_nt4(enc, kmers, 16));
        IS(kmers.size(), 0);

        IS(gen_kmer_nt4<KmerType>("ATGCATAATGCATAAA"), numeric_limits<KmerType>::max());
    }

    // decode
    {
        string dec;
        decode_nt4(enc, dec);
        IS(dec, "ATGNCATAATGNCATA");
    }
}
