#include "TestFramework.h"
#include "TestConfig.h"

void TestFramework::content()
{
    {
        const char* cmd = "../src/ht2-convert -q -P --out-pe-itlv -i " TEST_SOURCE_DIR "/test1.fastq.gz " TEST_SOURCE_DIR "/test2.fastq.gz -o interleave";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/test_interleave.fastq.gz", "interleave.fastq.gz"));
    }
    {
        const char* cmd = "../src/ht2-convert -q -P --pe-itlv -i " TEST_SOURCE_DIR "/test_interleave.fastq.gz -o non_interleave";
        ok(!system(cmd), cmd);
        OK(text_file_eq(TEST_SOURCE_DIR "/test1.fastq.gz", "non_interleave_1.fastq.gz"));
        OK(text_file_eq(TEST_SOURCE_DIR "/test2.fastq.gz", "non_interleave_2.fastq.gz"));
    }
}
