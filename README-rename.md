ht2-rename - generate simple sequence IDs by batch
==================================================

Introduction
------------

The original sequence ID usually contains detailed information and is very long.
ht2-rename generates short ID by auto-incremented numbers and user-defined
prefix|suffix.

Input and Output
----------------

The program don't distinguish single-end and paired-end inputs. If the input
sequences are paired-end in separate fils, just run the program on each file.

Usage
-----

Generates IDs like PRE_1_SUFF, PRE_2_SUFF, PRE_3_SUFF...:

    $ ht2-rename -i in_1.fastq -o renamed_1.fastq --prefix PRE_ --suffix _SUFF
