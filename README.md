HTQC - a high-throughput sequencing quality control toolkit
===========================================================

Introduction
------------

HTQC is a read quality control toolkit for high-throughput sequencing. It
contain a program for quality statistics, and several programs for quality
filtration.

Currently, only Illumina sequencing platform is supported.

System requirements
-------------------

- **Zlib** is required for build and run the programs.
- **Perl** and **Chart::Clicker** are required to run "ht2-stat-draw.pl", which
  renders the output tables of "ht2-stat" to charts.

If you build HTQC from source:

- The C++ compiler must have well-support on C++11. This means at least you need
  GCC 4.8, clang 3.3 or MSVC 2010. Preferably, you should use GCC 4.9, clang 3.5
  or MSVC 2013.

- CMake is used for cross-platform build configuration.

If your system don't have those softwares installed, please refer to your OS's
package management system. For example: *yum* for Fedora; *apt-get*, *aptitude*
and *synaptics* for Debian; *pacman* for Arch. Or you can visit their official
website:

-
-
- http://www.cmake.org

Install
-------

See "INSTALL" document.

List of Programs
----------------

- ht2-demul

  separate reads into individual files by barcode sequence.

- ht2-filter

  filter reads by quality / length / tile ID.

- ht2-overlap

  concatenate paired-end reads into single sequences.

- ht2-primer-trim

  remove primer sequences from reads.

- ht2-rename

  give sequences short name using auto-increased number and user-specified
  prefix & suffix.

- ht2-sample

  randomly pick some sequences.

- ht2-stat

  generate reads quality statistics report.

- ht2-stat-draw.pl

  draw charts from ht2-stat output.

- ht2-trim

  trim reads from start and/or end by quality.

For detailed descriptions, see individual README-XXX files for each program.
Run a program with "-h" or "--help" will show command-line options.

Typical usage
-------------

First of all, to know whether the sequencing reads are good:

    $ ht2-stat -P -i reads_R1_* reads_R2_* -o report_dir
    $ ht2-stat-draw.pl --dir report_dir

Suppose it shows tile 5 and 14 is bad. Remove reads from these tiles:

    $ ht2-lane-tile -i reads_R1_* --tile 5 14 -o tile_tidy_1
    $ ht2-lane-tile -i reads_R2_* --tile 5 14 -o tile_tidy_2

Trim low-quality tails:

    $ ht2-trim -i tile_tidy_1.fastq -o trim_1.fastq
    $ ht2-trim -i tile_tidy_2.fastq -o trim_2.fastq

Remove reads that are too short:

    $ ht2-filter -i trim_1.fastq trim_2.fastq -o filt

Maybe you want to concatenate paired-ends to longer sequences:

    $ ht2-overlap -i filt_1.fastq filt_2.fastq -o overlap -u non_overlap

Single-end or pair-end
----------------------

Some programs handle single-end and paired-end reads differently. For those
programs, outputs files are specified by a prefix, and multiple files will
generated. For "ht2-filter", when one end of a paired-end is rejected but the
other end is accepted, it is stored to "PREFIX_s.fastq".

Programs like "ht2-trim" don't distinguish between paired-end or single-end
mode. It only accepts one input file and one output file. You should run them
twice for paired-end reads, one time for the file of each end.

Reference
---------

We would be really appreciated if you cite our article:

Yang X, Liu D, Liu F, Wu J, Zou J, Xiao X, Zhao F, Zhu B.
HTQC: a fast quality control toolkit for Illumina sequencing data.
BMC Bioinformatics. 2013 Jan 31;14:33

Acknowledgements
----------------

JUCE core library is used for part of the base system. It is a light-weighted
library whose source can be contained and compiled with our project. Thanks for
the developers of JUCE! It's great!!!

Contact
-------

If you have any questions or find any bugs, please email us:

developer:

- Xi Yang: jiandingzhe@163.com, yangx@im.ac.cn
- Di Liu: liud@im.ac.cn

corresponding author:

- Baoli Zhu: zhubaoli@im.ac.cn
