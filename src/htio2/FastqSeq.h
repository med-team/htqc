//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTIO2_FASTQ_SEQ_H
#define	HTIO2_FASTQ_SEQ_H

#include <htio2/SimpleSeq.h>

namespace htio2
{

/**
 * @brief sequence with ASCII quality
 */
class FastqSeq : public SimpleSeq
{
public:
    typedef SmartPtr<FastqSeq> Ptr;
    typedef SmartPtr<const FastqSeq> ConstPtr;

public:
    /**
     * create an empty sequence object
     */
    FastqSeq();

    virtual ~FastqSeq();

    /**
     * generate reverse complement sequence
     * @param result
     */
    void revcom(FastqSeq& result) const;


    /**
     * Get part of the sequence. The quality is truncated correspondingly.
     * @param result store the sub-sequence
     * @param start start of the sub-sequence. 0 for the first base
     * @param length length of the sub-sequence. std::string::npos to get everyting until the end.
     */
    void subseq(FastqSeq& result, size_t start, size_t length = std::string::npos) const;

    /**
     * Stringified quality score. If you want quality in numbers, see function
     * "decode_quality" in header "QualityUtil.h".
     */
    std::string quality;
};

} // namespace htio2

#endif	/* HTIO2_FASTQ_SEQ_H */

