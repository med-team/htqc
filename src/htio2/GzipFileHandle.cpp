//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "htio2/GzipFileHandle.h"
#include "htio2/Error.h"

#include <cstring>
#include <cerrno>
#include <cstdio>
#include <zlib.h>

using namespace std;

namespace htio2
{

GzipFileHandle::GzipFileHandle(const string& file, IOMode mode) : auto_close(true)
{
    handle = gzopen(file.c_str(), get_io_mode_str(mode));
    if (handle == 0)
    {
        const char* msg = strerror(errno);
        throw runtime_error(string(msg));
    }
    offset = gztell(handle);
    curr_chr = gzgetc(handle);
}

GzipFileHandle::GzipFileHandle(gzFile handle, bool auto_close) : auto_close(auto_close)
{
    offset = gztell(handle);
    curr_chr = gzgetc(handle);
}

GzipFileHandle::~GzipFileHandle()
{
    if (auto_close) gzclose(handle);
}

bool GzipFileHandle::read_line(std::string& buffer)
{
    buffer.clear();
    if (curr_chr == EOF) return false;

    while (1)
    {
        switch (curr_chr)
        {
        case EOF:
            goto END;
        case LINE_END_CR:
            curr_chr = gzgetc(handle);
            if (curr_chr == LINE_END_LF) curr_chr = gzgetc(handle);
            goto END;
        case LINE_END_LF:
            curr_chr = gzgetc(handle);
            goto END;
        default:
            buffer.push_back(curr_chr);
            curr_chr = gzgetc(handle);
        }
    }

END:
    offset = gztell(handle) - 1;
    return true;
}

void GzipFileHandle::write_line(const std::string& content)
{
    gzwrite(handle, content.data(), content.size());
    gzputc(handle, '\n');
    offset = gztell(handle);
}

void GzipFileHandle::seek(off_t offset, int whence)
{
    if (whence == SEEK_SET)
    {
        this->offset = offset;
        gzseek(handle, offset, SEEK_SET);
    }
    else if (whence == SEEK_CUR)
    {
        gzseek(handle, this->offset, SEEK_SET);
        gzseek(handle, offset, SEEK_CUR);
        this->offset += offset;
    }
    else
        throw runtime_error("invalid seek whence");

    curr_chr = gzgetc(handle);
}

off_t GzipFileHandle::tell() const
{
    return offset;
}

} // namespace htio2
