#ifndef HTIO2_ERROR_H
#define	HTIO2_ERROR_H

#include <stdexcept>

#include "htio2/Enum.h"

namespace htio2
{

class InvalidQualityEncode : public std::invalid_argument
{
public:
    InvalidQualityEncode(QualityEncode encode);
    virtual ~InvalidQualityEncode() throw ();
};

class InvalidHeaderFormat : public std::invalid_argument
{
public:
    InvalidHeaderFormat(HeaderFormat format);
    virtual ~InvalidHeaderFormat() throw ();
};

class InvalidCompressFormat : public std::invalid_argument
{
public:
    InvalidCompressFormat(CompressFormat comp);
    virtual ~InvalidCompressFormat() throw ();
};

class InvalidSeqFormat : public std::invalid_argument
{
public:
    InvalidSeqFormat(SeqFormat format);
    virtual ~InvalidSeqFormat() throw ();
};

class InvalidRevcom : public std::invalid_argument
{
public:
    InvalidRevcom(const std::string& seq);
    virtual ~InvalidRevcom() throw ();
};

class InvalidFileContent : public std::runtime_error
{
public:
    InvalidFileContent(const std::string& content, size_t offset);
    virtual ~InvalidFileContent() throw ();
};

class InvalidKmerSize: public std::runtime_error
{
public:
    InvalidKmerSize(size_t size, size_t limit);
    virtual ~InvalidKmerSize() throw();
};

} // namespace htio2

#endif	/* HTIO2_ERROR_H */

