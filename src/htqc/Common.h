//     HTQC - a high-throughput sequencing quality control toolkit
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef HTQC_COMMON_H
#define HTQC_COMMON_H

#include "ht_config.h"
#include "htio2/Enum.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <vector>
#include <map>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <stdint.h>

#define CACHE_LINE_ALIGNMENT __attribute__ ((aligned (32)))

namespace htqc
{

extern const int LOG_BLOCK;
extern const size_t NUM_GUESS;

std::string bool_to_string(bool value);

void check_output_overwrite(const std::vector<std::string>& files_in,
                            const std::string& file_out);

void separate_paired_files(const std::vector<std::string>& in,
                           std::vector<std::string>& names_a,
                           std::vector<std::string>& names_b);

void add_fastq_suffix_se(const std::string& prefix,
                         htio2::CompressFormat comp,
                         std::string& file);

void add_fastq_suffix_pe(const std::string& prefix,
                         htio2::CompressFormat comp,
                         std::string& file1,
                         std::string& file2);

}


#endif
