//     HTQC - a high-throughput sequencing quality control toolkit
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTQC_MICROASSEMBLER_H
#define	HTQC_MICROASSEMBLER_H

#define USE_encode

#include <string>
#include <vector>

#include <stdint.h>

namespace htqc
{

typedef uint64_t KmerT;
typedef int16_t PosT;

class MicroAssembler
{
public:
    MicroAssembler(int kmer_size, int confident_quality, int aln_identity, int conflict_quality_diff);
    ~MicroAssembler();

    /// @return true if assembled
    bool assemble(const std::string& seq1,
                  const std::vector<int16_t>& qual_list_1,
                  const std::string& seq2,
                  const std::vector<int16_t>& qual_list_2);


    std::string result_seq;
    std::vector<int16_t> result_qual;
    std::string result_desc;
private:
    MicroAssembler();
    MicroAssembler(const MicroAssembler& orig);
    MicroAssembler& operator=(const MicroAssembler& orig);

    void gen_qual_mask(const std::vector<int16_t>& quality, std::vector<bool>& mask);

    int km_sz;
    int conf_qual;
    int aln_iden;
    int qual_diff;
};

} // namespace htqc
#endif	/* HTQC_MICROASSEMBLER_H */

