ht2-demul - separate sequences by lane ID and barcode sequence
=============================================================

Introduction
------------

Many high-throughput sequencing platforms allow multiple samples being sequenced
together, and distinguish the samples using known short oligonucleotides
(usually named "index" or "barcode). Therefore, it is necessary to separate
reads from different samples after you get the raw sequencing data, and this
operation is called **demultiplexing**.

The program `ht2-demul` separates reads from different samples using barcode
sequences stored in FASTQ header line.

Input and output
----------------

The input sequences can be single-end or paired-end. The sequence header must be
CASAVA 1.8 format, because prior to CASAVA version 1.8, the barcode
sequence is not stored in the headers.

The barcode file should be a tab-delimited plain text with 3 columns, which are:

  - project name
  - sample name
  - barcode sequence

Demultiplexed reads are firstly grouped into folders according to project name,
then by files according to sample name, so it is better to not include spaces in
your project name and sample name.

Usage
-----

Results are written to directory "output":

    $ ht2-demul --paired-end --barcode barcodes.tab -i IN_1.fastq IN_2.fastq -o output
