#include "htio2/MT19937.h"
#include "TestFramework.h"

#include <vector>
#include <cstring>

using namespace htio2::juce;

//
// the reference implementation of 64-bit MT19937 by Takuji Nishimura and Makoto Matsumoto.
// Copyright (C) 2004, 2014, Makoto Matsumoto and Takuji Nishimura, All rights reserved.
//

#define NN 312
#define MM 156
#define MATRIX_A 0xB5026F5AA96619E9ull
#define UM 0xFFFFFFFF80000000ull
#define LM 0x7FFFFFFFull

static uint64_t mt[NN];
static int mti=NN+1;

void init_genrand64(uint64_t seed)
{
    mt[0] = seed;
    for (mti=1; mti<NN; mti++)
        mt[mti] =  (6364136223846793005ull * (mt[mti-1] ^ (mt[mti-1] >> 62)) + mti);
}

void init_by_array64(uint64_t init_key[],
                                    uint64_t key_length)
{
    unsigned int i, j;
    uint64_t k;
    init_genrand64(19650218ull);
    i=1; j=0;
    k = (NN>key_length ? NN : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 62)) * 3935559000370003845ull))
          + init_key[j] + j; /* non linear */
        i++; j++;
        if (i>=NN) { mt[0] = mt[NN-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=NN-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 62)) * 2862933555777941757ull))
          - i; /* non linear */
        i++;
        if (i>=NN) { mt[0] = mt[NN-1]; i=1; }
    }

    mt[0] = 1ull << 63; /* MSB is 1; assuring non-zero initial array */
}

uint64_t genrand64_int64(void)
{
    int i;
    uint64_t x;
    static uint64_t mag01[2]={0ull, MATRIX_A};

    if (mti >= NN) { /* generate NN words at one time */

        /* if init_genrand64() has not been called, */
        /* a default initial seed is used     */
        if (mti == NN+1)
            init_genrand64(5489ull);

        for (i=0;i<NN-MM;i++) {
            x = (mt[i]&UM)|(mt[i+1]&LM);
            mt[i] = mt[i+MM] ^ (x>>1) ^ mag01[(int)(x & 1ull)];
        }
        for (;i<NN-1;i++) {
            x = (mt[i]&UM)|(mt[i+1]&LM);
            mt[i] = mt[i+(MM-NN)] ^ (x>>1) ^ mag01[(int)(x & 1ull)];
        }
        x = (mt[NN-1]&UM)|(mt[0]&LM);
        mt[NN-1] = mt[MM-1] ^ (x>>1) ^ mag01[(int)(x & 1ull)];

        mti = 0;
    }

    x = mt[mti++];

    x ^= (x >> 29) & 0x5555555555555555ull;
    x ^= (x << 17) & 0x71D67FFFEDA60000ull;
    x ^= (x << 37) & 0xFFF7EEE000000000ull;
    x ^= (x >> 43);

    return x;
}

int64_t genrand64_int63(void)
{
    return (int64_t)(genrand64_int64() >> 1);
}

double genrand64_real1(void)
{
    return (genrand64_int64() >> 11) * (1.0/9007199254740991.0);
}

double genrand64_real2(void)
{
    return (genrand64_int64() >> 11) * (1.0/9007199254740992.0);
}

double genrand64_real3(void)
{
    return ((genrand64_int64() >> 12) + 0.5) * (1.0/4503599627370496.0);
}


void TestFramework::content()
{
    uint64_t seed = Time::getCurrentTime().toMilliseconds();
    printf("seed: %lu\n", seed);

    htio2::MT19937 prng(seed);
    init_genrand64(seed);

    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

    is(genrand64_int64(), prng.next_uint64(), "uint64");
    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

    is(genrand64_int63(), prng.next_int63(), "positive int64");
    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

    is(genrand64_real1(), prng.next_double_yy(), "double inclusive");
    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

    is(genrand64_real2(), prng.next_double_yn(), "double right exclusive");
    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

    is(genrand64_real3(), prng.next_double_nn(), "double both exclusive");
    OK(std::memcmp(mt, prng.mt, sizeof(mt)) == 0);
    IS(mti, prng.mti);

#define NUM_CYCLE 1000000
    {
        int32_t sum = 0;
        for (int32_t i = 0; i < NUM_CYCLE; i++)
        {
            sum += prng.next_bool();
        }

        printf("%d of %d\n", sum, NUM_CYCLE);
    }

    {
        std::vector<size_t> sum(10);
        for (size_t i = 0; i < NUM_CYCLE; i++)
        {
            sum[prng.next_uint64_in_range(10)]++;
        }

        for (int i = 0; i < 10; i++)
        {
            printf("%lu\n", sum[i]);
        }
    }
}
