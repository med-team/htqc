HTIO - high-throughput sequence processing library
==================================================

Introduction
------------

HTIO is a library for processing high-throughput sequences. It contains
functions for FASTA/FASTQ file read/write, quality score decoding, FASTQ header
parsing, k-mer generation etc..

Synopsis
--------

    #include <htio2/FastqIO.h>
    #include <htio2/FastqSeq.h>
    //......

Usage
-----

To use HTIO library, you need to include corresponding header files in your
source code:

    #include <htio2/FastqIO.h>
    #include <htio2/FastqSeq.h>
    #include <htio2/Kmer.h>
    //......

To compile your program, you need to include the directory containing HTIO
header files, and link with HTIO library:

    $ g++ -o my_program -I MY/HEADER/PREFIX my_program.cpp MY/PATH/TO/libhtio2.a

By default, HTIO headers are installed to `/usr/local/include`, and the static
libraries are installed to `/usr/local/lib` (or `/usr/local/lib64` for 64-bit
*Fedora* systems). So you probably don't need to specify the `-I` option:

    $ g++ -o my_program my_program.cpp /usr/local/lib/libhtio1.a

Examples
--------

Convert sequences from FASTQ to FASTA:

    #include <htio1/FastqIO.h>
    #include <htio1/FastaIO.h>
    #include <htio1/FastqSeq.h>

    int main(int argc, char** argv)
    {
        // create file handles
        htio::FastqIO fh_in(argv[1], htio::HTIO_READ);
        htio::FastaIO fh_out(argv[2], htio::HTIO_WRITE);

        // read and write sequences
        htio::FastqSeq seq;
        while (fh_in.next_seq(seq))
            fh_out.write_seq(seq);
    }

Show positions where qualitiy is lower than 20:

    #include <htio1/FastqIO.h>
    #include <htio1/FastqSeq.h>
    #include <htio1/QualityUtil.h>
    #include <iostream>

    int main(int argc, char** argv)
    {
        // create file handles
        htio::FastqIO fh_in(argv[1], htio::HTIO_READ);

        // read sequences
        htio::FastqSeq seq;
        while (fh_in.next_seq(seq))
        {
            std::cout << seq.id << std::endl;

            // decode quality
            std::vector<int16_t> quality_dec;
            htio::decode_quality(seq.quality, quality_dec);

            // show quality
            for (size_t i=0; i<quality_dec.size(); i++)
            {
                if (quality_dec[i]<20)
                    std::cout << i+1 << ": " << quality_dec[i] << std::endl;
            }
        }
    }

Generate k-mers:

    #include <htio1/FastqIO.h>
    #include <htio1/FastqSeq.h>
    #include <htio1/Kmer.h>
    #include <iostream>

    int main(int argc, char** argv)
    {
        // create file handles
        htio::FastqIO fh_in(argv[1], htio::HTIO_READ);

        // read sequences
        htio::FastqSeq seq;
        while (fh_in.next_seq(seq))
        {
            // convert atgcn to 01234
            htio::EncodedSeq enc_seq;
            htio::encode_nt(seq.seq, enc_seq);

            // generate kmers, using a word size of six
            htio::KmerList kmers;
            htio::gen_kmer_nt(enc_seq, kmers, 6);

            // summarize occurred position
            htio::KmerPosMap kmer_pos;
            htio::summ_kmer_pos(kmers, kmer_pos);

            // show kmer and position
            for (htio::KmerPosMap::iterator it = kmer_pos.begin(); it != kmer_pos.end(); it++)
                std::cout << "kmer " << htio::decode_kmer_nt(it->first, 6) << " at " << it->second+1 << std::endl;
        }
    }
