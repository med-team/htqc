//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTIO2_FASTQ_IO_H
#define HTIO2_FASTQ_IO_H

#include "htio2/SeqIO.h"

namespace htio2
{

class FastqSeq;
class FastqIO;

class FastqIO : public SeqIO
{
public:
    typedef SmartPtr<FastqIO> Ptr;
    typedef SmartPtr<const FastqIO> ConstPtr;

public:
    /**
     * create sequence IO object by file name
     * @param file file to open
     * @param mode read/write/append
     * @param comp compress format. COMPRESS_UNKNOWN for automatically guess.
     */
    FastqIO(const std::string& file, IOMode mode, CompressFormat comp = COMPRESS_UNKNOWN);

    /**
     * create sequence IO object from HTIO file handle object
     * @param handle
     */
    FastqIO(FileHandle* handle);

    /**
     * create sequence IO object from C file handle
     * @param handle
     * @param auto_close if set to true, close the handle when sequence IO object is destroyed
     */
    FastqIO(FILE* handle, bool auto_close = true);

    /**
     * create sequence IO object from Zlib file handle
     * @param handle
     * @param auto_close if set to true, close the handle when sequence IO object is destroyed
     */
    FastqIO(gzFile handle, bool auto_close = true);
    virtual ~FastqIO();

    /**
     * read one sequence.
     * @param output
     * @return true if got a seq, false if reached end of file
     */
    virtual bool next_seq(FastqSeq& output);

    /**
     * read one sequence with quality
     * @param output
     * @return true if got a seq, false if reached end of file
     */
    virtual bool next_seq(SimpleSeq& output);

    /**
     * write one sequence
     * @param seq seq to write
     */
    virtual void write_seq(const FastqSeq& seq);

    /**
     * Write one sequence. Quality will be filled with 'I'
     * @param output
     */
    virtual void write_seq(const SimpleSeq& seq);

private:
    bool read_one_seq(std::string& id, std::string& desc, std::string& seq, std::string& qual);
};

} // namespace htio2

#endif // HTIO2_FASTQ_IO_H
