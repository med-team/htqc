#include "htio2/FastqIO.h"
#include "htio2/FastqSeq.h"
#include "htio2/HeaderUtil.h"
#include "TestFramework.h"
#include "TestConfig.h"

void TestFramework::content()
{
    htio2::FastqIO io(TEST_SOURCE_DIR"/test1.fastq", htio2::READ, htio2::COMPRESS_PLAIN);
    htio2::FastqSeq seq;
    OK(io.next_seq(seq));

    htio2::HeaderFormat format = htio2::HEADER_UNKNOWN;
    bool with_ncbi = false;
    htio2::guess_header_format(seq.id+" "+seq.desc, format, with_ncbi);
    IS(format, htio2::HEADER_1_8);
    OK(!with_ncbi);

    htio2::guess_header_format("SRR000000 "+seq.id+" "+seq.desc+" length=100", format, with_ncbi);
    IS(format, htio2::HEADER_1_8);
    OK(with_ncbi);

    format = htio2::HEADER_UNKNOWN;
    with_ncbi = false;
    htio2::guess_header_format(io, 5, format, with_ncbi);
    IS(format, htio2::HEADER_1_8);
    OK(!with_ncbi);

    {
        htio2::HeaderParser header_parser(htio2::HEADER_1_8, false);
        header_parser.parse(seq.id, seq.desc);
        IS(header_parser.lane, 1);
        IS(header_parser.tile, 1101);
        IS(header_parser.barcode, "CTTGTA");
    }

}
