ht2-convert - convert reads storage format
==========================================

Introduction
------------

The program `ht2-convert` can convert several aspects for FASTQ format
sequences, including:

  - quality encode
  - paired-end interleaving
  - strand

### Paired-end Interleaving

There are two common ways to store paired-end files. One way is to store them in
separate file pairs, each containing one end:

    File 1:      File 2:
    r1_1         r1_2
    r2_1         r2_2
    r3_1         r3_2

and the other way is to store them in one file, which is called **interleaving**:

    File:
    r1_1
    r1_2
    r2_1
    r2_2
    r3_1
    r3_2

### Strand

In most common cases, paired-end sequences has following layout:

    >>>> read 1 >>>>
    =========================== sequence ===========================
                                                    <<<< read 2 <<<<

However, for long insertion size libraries, the paired-end sequences may have
this layout:

                                                >>>> read 1 >>>>
    =========================== sequence ===========================
        <<<< read 2 <<<<

-----
Usage
-----

Separate interleaved file:

    $ ht2-convert -P --pe-itlv -i input.fastq -o output

files "output_1.fastq" and "output_2.fastq" will be generated.

Combine file pairs into interleaved file:

    $ ht2-convert -P --out-pe-itlv -i input_1.fastq input_2.fastq -o output

the file "output.fastq" will be generated.
