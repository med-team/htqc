ht2-overlap - assemble overlapping read pairs
=============================================

Introduction
------------

Merge 3'-overlapping paired-end reads into single sequences.

Input and output
----------------

Input should be paired-end reads. If read ends are stored in file pairs, all
files of read 1 should be given firstly, then follwing all files of read 2.

The assembled reads are written into one file. The non-assembled reads are
written into file specified by "-u" option.

Usage
-----

Default run:

    $ ht2-overlap -i IN_1.fastq IN_2.fastq -o overlap

If you also need those non-assembled reads, provide a file prefix using "-u"
option:

    $ ht2-overlap -i IN_1.fastq IN_2.fastq -o overlap -u non_overlap

Files `non_overlap_1.fastq` and `non_overlap_2.fastq` will be generated.

Assume your input reads are separated into multiple volumes. You can specify
those input files via file globbing:

    $ ls
    IN_vol1_1.fastq    IN_vol2_1.fastq    IN_vol3_1.fastq
    IN_vol1_2.fastq    IN_vol2_2.fastq    IN_vol3_2.fastq
    $ht2-overlap -i IN_*_1.fastq IN_*_2.fastq -o overlap -u non_overlap
