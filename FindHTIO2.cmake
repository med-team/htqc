include(FindPackageHandleStandardArgs)
include(GNUInstallDirs)

set(HTIO2_ROOT "" CACHE PATH "Additional HTIO2 search path")

# find HTIO2 config header
find_file(htio2_config_h htio2/config.h
    PATHS ${HTIO2_ROOT}
    PATH_SUFFIXES ${CMAKE_INSTALL_INCLUDEDIR})

#message(STATUS "  config header: ${htio2_config_h}")

# parse version
if(htio2_config_h)
    file(STRINGS ${htio2_config_h} line_major REGEX "VERSION_MAJOR")
    file(STRINGS ${htio2_config_h} line_minor REGEX "VERSION_MINOR")
    file(STRINGS ${htio2_config_h} line_patch REGEX "VERSION_PATCH")

    string(REGEX REPLACE "^.*VERSION_MAJOR +([0-9]+).*$" "\\1" HTIO2_VERSION_MAJOR "${line_major}")
    string(REGEX REPLACE "^.*VERSION_MINOR +([0-9]+).*$" "\\1" HTIO2_VERSION_MINOR "${line_minor}")
    string(REGEX REPLACE "^.*VERSION_PATCH +([0-9]+).*$" "\\1" HTIO2_VERSION_PATCH "${line_patch}")
    set(HTIO2_VERSION_STRING "${HTIO2_VERSION_MAJOR}.${HTIO2_VERSION_MINOR}.${HTIO2_VERSION_PATCH}")
    get_filename_component(HTIO2_INCLUDE_DIRS ${htio2_config_h} PATH CACHE)
endif()

# find HTIO2 library
find_library(htio2_archive htio2
    PATHS ${HTIO2_ROOT}
    PATH_SUFFIXES ${CMAKE_INSTALL_LIBDIR})

if(htio2_archive)
    set(HTIO2_LIBRARIES ${htio2_archive} CACHE STRING "HTIO2 library")
endif()

#message(STATUS "  library: ${htio2_archive}")

FIND_PACKAGE_HANDLE_STANDARD_ARGS(HTIO2
    REQUIRED_VARS htio2_config_h htio2_archive
    VERSION_VAR HTIO2_VERSION_STRING)
