#ifndef HTIO2_ENUMS_H
#define	HTIO2_ENUMS_H

#include <string>

namespace htio2
{

typedef enum
{
    STRAND_UNKNOWN = 0,
    STRAND_FWD = 1,
    STRAND_REV = 2,
} Strand;

typedef enum
{
    HEADER_UNKNOWN = 0,
    HEADER_PRI_1_8 = 1, ///< header format prior to CASAVA 1.8
    HEADER_1_8 = 2, ///< header format by CASAVA 1.8
} HeaderFormat;

typedef enum
{
    ENCODE_UNKNOWN = 0,
    ENCODE_SANGER = 1, ///< 0 to 93 using ASCII 33 to 126
    ENCODE_SOLEXA = 2, ///< -5 to 62 using ASCII 59 to 126
    ENCODE_ILLUMINA = 3, ///< 0 to 62 using ASCII 64 to 126
    ENCODE_CASAVA_1_8 = 4, ///< 2 to 40 using ASCII 35 to 73
} QualityEncode;

typedef enum
{
    FORMAT_UNKNOWN = 0,
    FORMAT_FASTQ = 1,
    FORMAT_FASTA = 2,
} SeqFormat;

typedef enum
{
    COMPRESS_UNKNOWN = 0,
    COMPRESS_PLAIN = 1,
    COMPRESS_GZIP = 2,
    COMPRESS_BZIP2 = 3,
    COMPRESS_LZMA = 4,
} CompressFormat;

typedef enum
{
    READ,
    WRITE,
    APPEND
} IOMode;

} // namespace htio2

#endif	/* HTIO2_ENUMS_H */

