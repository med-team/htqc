Changes
=======

1.92.3
======

  - htio2 library

    Fixed bugs on missing move constructor of Option class, and now it works
    properly with GCC 4.7.

1.92.2
======

  - Refined unit test for ht2-overlap.

  - htio2 library

    Fixed a bug on command-line option parser, where values with leading "-"
    was recognized as option key.

1.92.1
======

  - ht2-overlap:

    Fixed a severe bug that overlapping part in result sequence all come from
    read 1, and probably using incorrect position.

1.92.0
======

  - Use *JUCE* library to provide utility functions, and removed dependency on
    *boost* whose CMake finder module is problematic. The *JUCE* stuffs are put
    inside `htio2::juce` namespace.

  - htio2 library

    Added string manipulation functions in `StringUtil.h`: `split`, `join`,
    `to_upper_case` and `to_lower_case`.

    Added a command line option parser facility in `OptionParser.h`.

    Replaced boost::intrusive_ptr by self-written pointer type. The Smart
    pointer types for each class is defined inside the class. For example,
    `FooBarPtr` are redefined to `FooBar::Ptr`.

    Removed dependency on regular expression engine. Header format is guessed
    using manually-written parser.

    Modify the prefix of header file guardian macros from `HTIO_` to `HTIO2_`.

  - all programs

    Using `htio2::OptionParser` to parse command line options, which replaces
    `boost::program_options`. Some option names are slightly modified.

    Set default quality encoding from casava1.8 to unknown, which let the
    program to guess sequence quality encoding.

  - ht2-demul

    Fixed a severe bug that when working with paired-end reads, read 1 is
    written to both output.

    Added unit test for read end validation.

  - ht-stat

    Fixed a bug that when working with very few amount of paired-end reads, the
    QQ plot sampling would fall into endless cycle.

    Use lock-free ring buffers to pass information between threads.

  - ht2-sample

    Now get accurate amount of results, instead of approximate amount.

    Can directly specify number, or specify fraction of total.

    Can write the indices of selected reads (or read pairs) into a file.

1.91.0
======
  - use GNUInstallDirs to provide correct install location.

  - finished ht2-filter and ht2-lane-tile.

  - use markdown format for all documents.

1.90.1
======

  - all programs:

    The "--gzip/-z" option no longer exists. Compression is deduced by input
    file name.

  - ht2-filter and ht2-lane-tile-pick

    Filter by length and quality at one time: pick those sequence whose length
    of high quality bases is longer than length cutoff. The function of tile
    filter is separated to ht2-lane-tile-pick.

1.90.0
======

This is the pre-release of version 2.

  - htio library

    Header files are placed in htio2/ directory.

    Added a new "RefCounted" base class which provides refcount. All
    refcounted classes use Boost's intrusive_ptr class to provide a default
    implementation for smart pointer.

    Added a new "PairedEndIO" class which read/write paired-end reads, and
    process issues such as interleaved / reverted mate pairs.

    Moved all sequence format utilities into "SeqFormat.h", and added
    functions to guess sequence format by file name or content.

    Added write_seq(const FastqSeq&) to SeqIO base class.

    Removed "HTIO_" prefix from all enums.

    Tidy header file inclusion to increase compile speed.

    Kmer utilities now support different memory size of Kmer type via template
    specialization. Default specializations includes 64, 32 and 16 bit unsigned
    integers.

    Fixed errors of seek with SEEK_CUR and SEEK_END for the file handle classes.

  - all programs

    Allow interleaved input/output for paired-end reads.
    
    Use output file prefix instead of file name, so that all programs have same
    behaviour.

  - ht2-convert

    a new program that converts sequence format, quality encoding, etc..

  - ht2-overlap

    the previous ht-asm program renamed to ht2-overlap to clarify its function.

  - ht2-primer-trim

    Supports multiple output by different primer set.

    Do check reverse-complement strand by default, and use "--no-revcom" option
    to disable this behaviour.

    Added "--no-trim" option to not trim the primer sequence from reads, and
    only pick the primer-containing reads.

0.90.8
======

  - ht-demul

    ".gz" suffix will be added for gzipped output files.

0.90.7
======

  - ht-qual-recode

    a new program for changing the encoding of sequence quality.

  - GZipFileHandle.h and SeqIO.h

    fixed an error that may cause conflicting type declaration of "gzFile", on
    some version of zlib.

0.90.6
======

  - ht-demul

    fixed a bug that the program crashes if the output directory is not exist.

    fixed a bug that the barcode file is not properly parsed.

  - htio library

    documentation for all functions.

    FindHTIO1.cmake.

    README_htio for simple tutorial on htio library.

    two new functions for kmer generation. The length of whole input string is
    treated as kmer size.

0.90.5
======

  - htio library

    removed #include in header files as much as possible. This would help to
    increase compile speed.

  - improved quality encode guessing strategy.

  - guess encoding and header format from front 500 sequences.

0.90.4
======

  - ht-primer-trim

    changed default cutoff for allowed tail missing from 3 to 5.

  - ht-asm

    fixed an error which would cause segfault on specific inputs.

  - added README for each program.

  - source code

    moved all C++ sources into a subdirectory so that the main directory would
    have a clean look.

0.90.3
======

  - htio library

    fixed an bug on tell() of PlainFileHandle and GzipFileHandle.

    fixed an bug on EncodedSeq revcom.

    allowing FastaIO to read and write FastqSeq.

0.90.2
======

  - htio library

    added functions htio::SeqIO::New(), that automatically guess sequence format
    from file content, and create handle for it.

    fixed an error on FileHandle that seek() will disturb the file offset.

    fixed an error on FastaIO that seek() will disturb the file offset. Now it
    is ensured after reading one sequence, the offset is at the start of the
    next sequence's header line.

0.90.1
======

  - htio library

    added operators on EncodedSeq and KmerList.

  - fixed a bug that ht-primer-trim won't be installed.

0.90.0
======

  - htio library

    the include directory and library name now renamed to "htio1", with revised
    API and ABI.

    add functions for guess quality encoding and header format.

    headers will be installed to PREFIX/include/htio1, and library is renamed to
    libhtio1

    quality range of CASAVA_1_8 changed to 0 - 41.

    line reader can recognize CR, LF and CRLF.

  - all programs

    automatically guess quality encoding and header format.

  - ht-asm

    the old "ht-join" program now renamed to "ht-asm".

  - ht-demul

    a new program to demultiplex reads by barcode. Barcodes are got from the
    FASTQ header.

  - ht-primer-trim

    a new program to remove primer sequence from reads.

  - ht-stat

    fixed a bug that the program crashes when the number of threads is more than
    the number of input sequences.

  - ht-stat-draw.pl

    refined the output of lane-tile plots.

    as GNUplot has issues on SVG heatmap, change default output format to PNG.

    removed default font face setting from 'Arial' to '*', in avoid that some
    machines don't have that font.

0.15.1
======

  - revised CMakeLists.txt

    might fix some bugs caused by library locating.

  - ht-rename

    by default, don't keep reads description (contents after first continuous
    blank).

0.15.0
======

  - all programs

    use Boost instead of Glib.

  - ht-rename

    a new program to generate simple sequence IDs by batch.

  - ht-stat

    don't need to specify maximum read length.

  - ht-filter

    now won't accept zero-length sequences when filtering by quality.

0.14.1
======

  - ht-stat

    fixed a severe error that the program stucks at the third input file, due to
    inappropriate queue access.

0.14.0
======

  - ht-trim

    require several continuous residues of good quality. If you want to fall
    back to old behavior, set word size to 1.

  - all programs

    show help when run without any parameters.

    check suffix for output file names and prefixes.

0.13.2
======

  - all programs

    add ".gz" suffix for output file names when using gzipped input and output.

    user must specify "--single" or "--paired" explicitly, instead of single
    mode by default.

  - ht-join

    specify output prefix for unjoined sequences using one single option "-u".

0.13.1
======

  - HTIO library

    fixed an error in FastqIO, that the program aborts when sequence has ID only
    and has no description.

  - added unit tests.

  - README and program help

    corrected some disaccording descriptions due to the upgrade from 0.12.X to
    0.13.X.

0.13.0
======

  - HTIO library

    major API change.

    added documentation.

  - ht-stat

    fixed a severe error that if multiple input files are given, the program
    halts after finishing the first file.

  - ht-filter

    combines ht_length_filter, ht_tile_filter, ht_qual_filter into one program.

  - all programs

    use "-" instead of "_" in program names, so all programs are named "ht-xxx"
    instead of "ht_xxx"

    use single option "-o" to specify output prefix, instead of using "-a" "-b"
    "-s" separately.

0.12.1
======

  - ht_join

    write unjoined sequences to user specified files (options -U -V).

0.12.0
======

  - source code

    IO classes and FASTQ sequence classes are moved to a new library named HTIO.

  - ht_qual_filter, ht_length_filter, ht_sample

    output for single-end mode now use "--out-s" instead of "--out-a".

  - ht_stat_draw.pl

    help document now provides correct usage.

  - added reference in README.

0.11.1
======

  - ht_stat

    fixed a bug that parameters are not correctly printed to info file.

  - ht_join

    fixed a bug that quality on assembled sequence tail was not generated
    correctly.

    write more information to output sequence header.

    aligning bases must both have good quality to be considered identical. This
    will make the alignment more stringent.

  - all programs

    print parameters on log.

    print log on STDOUT instead of STDERR.

0.11.0
======

  - ht_join

    a new program that joins paired-end reads into single sequences.

  - all programs

    use '--quiet' instead of '--verbose'. Thus all programs will by default
    print information, unless you explicitly let them quiet.

  - source code

    checked if the GPL v3 license exists in all source files.

    the command line option objects that have enumerative string values now use
    G_OPTION_ARG_CALLBACK type, so that main functions of related programs don't
    need to parse these options by themselves.

0.10.2
======

  - all programs

    support read/write gzipped sequences.

    exit with error message when failed to open a file.

  - source code

    modified file layout; move everything to "htqc" namespace.

    use C style stdio to read sequences instead of C++ stream.

0.10.1
======

  - ht_stat

    refined thread usage.

    fixed an error that the program behaved weirdly if there are too few input
    reads.

  - ht_length_filter, ht_qual_filter, ht_sample

    If the program did not work in paired-end mode ("--paired" or "-p"), but
    output files were provided for the other end ("--out-b" or "-b") or the
    unpaired ("--out-single" or "-s"), these programs will claim that.

  - all programs

    common option objects are compiled into a library. This is for the ease of
    development, and does not affect user experience.

0.10.0
======

  - all programs

    able to take multiple input files. Command-line options are modified
    accordingly.

  - ht_stat, ht_tile_filter

    control FASTQ quality encoding and header format using separate options
    ("--encode" and "--header-format").

  - ht_stat

    creates separate files for text reports instead of one big file, and "--out"
    option now specifies an output folder containing these files.

  - ht_trim

    this program replaces ht_tail_filter, and it can trim reads from both head
    and tail.

  - refined README and program help.

0.9.7
=====

  - ht_stat

    fixed the bug when -l is longer than actual read size, -65535 was given in
    some charts.

  - added a new program named "ht_sample" to pick sample from reads.

  - FASTQ parser

    modified '+' line validation.

0.9.6
=====

  - ht_stat

    threaded

  - re-organize source code

  - solved warnings about const char*

0.9.5
=====

  - ht_stat

    calculate Pearson correlation and print in QQ plot

  - ht_stat.pl

    changed line color for distribution of read quality and read length.

0.9.4
=====

  - ht_stat_draw.pl

    allow users to select picture format

  - print logs on stderr

0.9.3
=====

  - in addition of cycle quality heat map, add a box graph

  - correct descriptions of program helps

0.9.2
=====

  - a new perl script for rendering ht_stat outputs using gnuplot

  - added '-v' or '--version' command-line switch to show program version

0.9.1
=====

  - filled bugs in regexp for illumina header

0.9.0
=====

  - initial release
