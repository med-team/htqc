//     HTIO - a high-throughput sequencing reads input/output library
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef HTIO2_HEADER_UTIL_H
#define HTIO2_HEADER_UTIL_H

#include "htio2/Enum.h"

namespace htio2
{

/**
 * Guess header format from one header.
 * @param header
 * @param format header format of the input header
 * @param with_ncbi_additive will be set to true if the header has information added by NCBI SRA
 */
void guess_header_format(const std::string& header, HeaderFormat& format, bool& with_ncbi_additive);


class FastqIO;

/**
 * Guess header format from several input sequences.
 * @param stream sequences to guess are read from this stream.
 * @param num_to_guess the amount of sequences to be guessed.
 * @param format header format of input sequences.
 * @param with_ncbi_additive will be set to true if the header has information added by NCBI SRA.
 * @return number of sequences that are actually used to guess.
 */
size_t guess_header_format(FastqIO& stream, size_t num_to_guess, HeaderFormat& format, bool& with_ncbi_additive);

class HeaderParser
{
public:
    /**
     * create a FASTQ header parser
     * @param h_format the header format
     * @param with_ncbi_additive whether the header would contain NCBI SRA additional contents.
     */
    HeaderParser(HeaderFormat h_format, bool with_ncbi_additive);

    /**
     * Parse a FASTQ header. The results are stored on the members of HeaderParser object.
     * @param id the content of FASTQ header before the first block of continuous space
     * @param desc the content of FASTQ header after the first block of continuous space
     */
    void parse(const std::string& id, const std::string& desc);

    HeaderFormat format;
    bool with_ncbi_additive;
    int lane_colon_pos;
    int lane;
    int tile;
    std::string barcode;
};

} // namespace htio2

#endif // HTIO2_HEADER_UTIL_H
