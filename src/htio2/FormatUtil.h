#ifndef HTIO2_FORMAT_UTIL_H
#define	HTIO2_FORMAT_UTIL_H

#include <string>
#include <htio2/Enum.h>

namespace htio2
{

class FileHandle;

CompressFormat guess_compress_by_name(const std::string& file);

SeqFormat guess_format_by_name_and_content(const std::string& file, CompressFormat comp);
SeqFormat guess_format_by_name(const std::string& file);
SeqFormat guess_format_by_content(FileHandle& handle);


} // namespace htio2

#endif	/* HTIO2_FORMAT_UTIL_H */

