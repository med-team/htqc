#ifndef HTIO2_SIMPLE_SEQ_H
#define	HTIO2_SIMPLE_SEQ_H

#include <string>

#include "htio2/RefCounted.h"

namespace htio2
{

/**
 * generate reverse complement nucleotide sequence
 * @param in input sequence
 * @param out output result
 */
void revcom(const std::string& in, std::string& out);

/**
 * @brief a simple sequence class with ID, description and sequence content
 */
class SimpleSeq : public RefCounted
{
public:
    typedef SmartPtr<SimpleSeq> Ptr;
    typedef SmartPtr<const SimpleSeq> ConstPtr;

public:
    /**
     * Create an empty sequence object
     */
    SimpleSeq();

    /**
     * create an sequence object with ID, description and sequence
     * @param id
     * @param desc
     * @param seq
     */
    SimpleSeq(const std::string& id, const std::string& desc, const std::string& seq);
    virtual ~SimpleSeq();

    /**
     * get sequence length
     * @return sequence length
     */
    inline size_t length() const
    {
        return seq.length();
    }
    /**
     * generate reverse complement sequence
     * @param result
     */
    void revcom(SimpleSeq& result) const;

    /**
     * @brief subseq
     * @param result
     * @param start
     * @param length
     */
    void subseq(std::string& result, size_t start, size_t length = std::string::npos) const;

    /**
     * get part of the sequence
     * @param result
     * @param start the start of sub sequence, 0 for the first base
     * @param length the length of sub sequence
     */
    void subseq(SimpleSeq& result, size_t start, size_t length = std::string::npos) const;

    std::string id;
    std::string desc;
    std::string seq;
private:

};

inline void SimpleSeq::subseq(std::string& result, size_t start, size_t length) const
{
    result.assign(seq, start, length);
}

} // namespace htio2

#endif	/* HTIO2_SIMPLE_SEQ_H */

