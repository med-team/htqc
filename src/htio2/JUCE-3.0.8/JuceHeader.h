/*

    IMPORTANT! This file is auto-generated each time you save your
    project - if you alter its contents, your changes may be overwritten!

    This is the header file that your files should include in order to get all the
    JUCE library headers. You should avoid including the JUCE headers directly in
    your own source files, because that wouldn't pick up the correct configuration
    options for your app.

*/

#ifndef __JUCE_HEADER_H__
#define __JUCE_HEADER_H__

#include "AppConfig.h"
#include "modules/juce_core/juce_core.h"

#endif   // __JUCE_HEADER_H__
